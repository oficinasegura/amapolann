<!--<meta charset="utf-8" />-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="description" content="" />
<meta name="author" content="Damian Gallo | www.damiangallo.com.ar  " />
<meta name="keywords" content="" />
<!-- Viewport para celulares: j.mp/bplateviewport -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="designer" content="Damian Gallo | www.damiangallo.com.ar  " />
<meta name="robots" content="all" />


<!-- <link rel='shortcut icon' type='image/vnd.microsoft.icon' href='img/favicon.ico'> --> <!-- IE -->
<link rel='apple-touch-icon' type='image/png' href='img/fav_57.png'> <!-- iPhone -->
<link rel='apple-touch-icon' type='image/png' sizes='72x72' href='img/fav_72.png'> <!-- iPad -->
<link rel='apple-touch-icon' type='image/png' sizes='114x114' href='img/fav_114.png'> <!-- iPhone4 -->
<link rel='icon' type='image/png' href='img/fav_114.png'> <!-- Opera Speed Dial, at least 144×114 px -->