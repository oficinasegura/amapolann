<header id="main-header">
	
	<div class="container-fluid">			
		

		<div class="row">
			
			<div class="col-xs-7 col-sm-10 col-md-12">

				<div id="contenedor_logo_circulo">
					<a href="#" title="" id="main-logo"><img src="img/logo.png" alt=""></a>
				</div>

				<ul id="main-menu" class="nav nav-pills hidden-xs">
					<li><a href="#" class="mis_tasaciones">MIS PROPIEDADES</a></li>
					<li><a href="#" class="edicion_campos">BUSCADOR</a></li>
					<li><a href="#" class="salir">SALIR</a></li>
				</ul>

			</div>


			<div class="col-xs-2 col-sm-2 col-lg-2 pull-right">				
				<a href="#" class="menu_hamburguesa ejemplo_cruz visible-xs"><span></span></a>
			</div>

		</div><!-- row -->

	</div><!-- container -->






	
	<div class="visible-xs">
		<div id="contiene_botonera_desplegable">

			<ul id="mobile-main-menu">				
				<li><a href="#" class="mis_tasaciones">MIS TASACIONES</a></li>
				<li><a href="#" class="edicion_campos">EDICIÓN DE CAMPOS</a></li>
				<li><a href="#" class="salir">SALIR</a></li>
			</ul>
		</div><!-- contiene_botonera_desplegable -->
	</div>


</header><!-- /header -->







