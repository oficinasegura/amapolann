$(document).ready(function() {
	anchor.init();
});

anchor = {
	init : function()  {
		$("a.anchorLink").click(function(){
			elementClick = $(this).attr("href");
			destination = $(elementClick).offset().top;
			alto_header = $("#main-header").height(); /*Le resto al alto del header porque queda fijo*/
			$("html:not(:animated),body:not(:animated)").animate({scrollTop: destination-alto_header}, {duration: 800, easing: "easeOutExpo"});
			return false;
		});
	}
};