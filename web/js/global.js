/*

 * Friendly Time JS

 * Calcular el friendly time a partir de un UNIX

 * @params integer posted

 */

function friendly_time(posted) {

        var diff = server_time - local_time;

                var now = Math.round(new Date().getTime() / 1000) + diff;

                var ago = now - posted;

                if(ago < 60) {

                               return 'justo ahora';

                }

                else if(ago < 3600) {

                               ago = Math.round(ago / 60);

                               if(ago == 0) {

                                               ago = 1;

                               }

                               if(ago > 1) {

                                               return 'hace '+ ago +' minutos';

                               }

                               return 'hace un minuto';

                }

                else if(ago < 86400) {

                               ago = Math.round(ago / 3600);

                               if(ago == 0) {

                                               ago = 1;

                               }

                               if(ago > 1) {

                                               return 'hace ' + ago + ' horas';

                               }

                               return 'hace una hora';

                }

                else {

                               ago = Math.round(ago / 86400);

                               if(ago == 0) {

                                               ago = 1;

                               }

                               if(ago > 1) {

                                               return 'hace '+ ago +' dias';

                               }

                               return 'ayer';

                }

}
var userid;
function getFillerFolderByUserId(userid, imgid){
    var filler;
    filler=Math.ceil(userid/1500);
    document.getElementById(imgid).src="http://static.guia.clarin.com/filer/userfiles/uf"+filler+"/"+userid+"/avatar";
    
}

function submitFacets(value){

    document.getElementById("facets").value = value;
    document.getElementById("filtros").submit('GET');
}

function removeFacets(value){
    var str= document.getElementById("facets").value;
    str=    str.replace(value, "");
    str=    str.replace("####", "##");
    document.getElementById("facets").value=str;
    document.getElementById("filtros").submit('GET');
    
}

function removeAspects(value){
    var str= document.getElementById("aspects").value;
    str=    str.replace(value, "");
    str=    str.replace("####", "##");
    document.getElementById("aspects").value=str;
    document.getElementById("filtros").submit('GET');
    
}

function removeDistance(value){
    var str= document.getElementById("distance").value;
    str=    str.replace(value, "");
    str=    str.replace("####", "##");
    document.getElementById("distance").value=str;
    document.getElementById("filtros").submit('GET');
    
}

function submitAspects(value){
  
    document.getElementById("aspects").value=document.getElementById("aspects").value +value+",";
    document.getElementById("filtros").submit('GET');
}

function submitDistance(value){
    document.getElementById("distance").value=value;
    document.getElementById("filtros").submit('GET');
}

function submitOrder(value){
    document.getElementById("order").value=value;
    document.getElementById("filtros").submit('GET');
}


function example_ajax_request() {
  $('body').addClass("loading"); 
}

