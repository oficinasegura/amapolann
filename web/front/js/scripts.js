$(window).on('load', function(){
    activo_botones_favoritos();
	lanzoModalTempral();

	toolTipCotizar();
	
});



function toolTipCotizar(){
	setTimeout(function(){
		$("p.tooltip-producto").addClass("visible");
	}, 300);
	
	setTimeout(function(){
		$("p.tooltip-producto").removeClass("visible");
	}, 5500);

	$("p.tooltip-producto a").on("click", function(event){
		event.preventDefault();
		$("p.tooltip-producto").removeClass("visible");
	});
}




function activo_botones_favoritos(){
	//alert("Se van a activar");
	$('.bt_agregar_favoritos').css("visibility","visible");
}



	//BLOQUE 1


	// menu responsive animacion

	var contador = 1;
	
	$('#menu-btn, .dropdown-menu-mobile a').click( function () {
		// $('nav').toggle(); Forma Sencilla de aparecer y desaparecer

		if (contador == 1) {

			$('.nav-fondo').show (0);
			$('html').addClass('mostrar-scroll-y');
			$('header').addClass('header-fixed'); 
			contador = 0;
		} else {
			$('.nav-fondo').hide (0);
			$('html').removeClass('mostrar-scroll-y');
		 $('header').removeClass('header-fixed');
            contador = 1;
		};

	});

	


	$(document).ready(function(){
		$('.dropdown-menu-mobile a').click(function(){
		  $('.menu-btn').prop('checked', false);
		});
	  });

        
  const mql = window.matchMedia('(min-width: 750px)');

mql.addEventListener('change', () => {

    $('.nav-fondo').hide(0);
    $('html').removeClass('mostrar-scroll-y');
    contador == 1

    this.checkNative();

  
});





window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 78 || document.documentElement.scrollTop > 78) {
	 $('#main-header').addClass('header-fixed')
  } else {
	$('#main-header').removeClass('header-fixed')
  }
}

$(document).ready(function(){//ACCION CUANDO CARGUE LA PAGINA

	var altura_arr = [];//CREAMOS UN ARREGLO VACIO
	$('.altura-automatica').each(function(){//RECORREMOS TODOS LOS CONTENEDORES DE LAS IMAGENES, DEBEN TENER LA MISMA CLASE
	  var altura = $(this).height(); //LES SACAMOS LA ALTURA
	  altura_arr.push(altura);//METEMOS LA ALTURA AL ARREGLO
	});
	altura_arr.sort(function(a, b){return b-a}); //ACOMODAMOS EL ARREGLO EN ORDEN DESCENDENTE
	$('.altura-automatica').each(function(){//RECORREMOS DE NUEVO LOS CONTENEDORES
	  $(this).css('height',altura_arr[0]);//LES PONEMOS A TODOS LOS CONTENEDORES EL PRIMERO ELEMENTO DE ALTURA DEL ARREGLO, QUE ES EL MAS GRANDE.
	});
  });


  $(document).ready(function(){

	var altura_arr = [];
	$('.altura-automatica-galeria').each(function(){
	  var altura = $(this).height(); 
	  altura_arr.push(altura);
	});
	altura_arr.sort(function(a, b){return b-a}); 
	$('.altura-automatica-galeria').each(function(){
	  $(this).css('height',altura_arr[0]);
	});
  });


  $(document).ready(function(){

	var altura_arr = [];
	$('.altura-automatica-terminaciones').each(function(){
	  var altura = $(this).height(); 
	  altura_arr.push(altura);
	});
	altura_arr.sort(function(a, b){return b-a}); 
	$('.altura-automatica-terminaciones').each(function(){
	  $(this).css('height',altura_arr[0]);
	});
  });





$(window).on('load', function(){

  var topDist = $(this).scrollTop();
  analiza_alto_para_bt_ir_arriba(topDist);
  //analiza_ancho_para_chat();

  activa_metodos_favoritos();

});

$(window).scroll(function(){
	var topDist = $(this).scrollTop();
	//console.log(topDist);
	analiza_alto_para_bt_ir_arriba(topDist);
});






/*Botón ir arriba----------------*/

function analiza_alto_para_bt_ir_arriba(alto_scroll){

    var alto_nav = $(window).innerHeight();

    if(alto_scroll > 300)
    {
        $("a#ir_arriba").addClass('visible');
    }
    else
    {
        $("a#ir_arriba").removeClass('visible');
    }
}

$(document).ready(function(){
    $("a#ir_arriba").on('click', function(event){
        event.preventDefault();
        // $('body,html').animate({scrollTop: 0}, {duration: 600, easing: "easeOutExpo"});
        // $('body,html').animate({ scrollTop:'0px' },1000);
		window.scrollTo({
			top: 0,
			// left: 0,
			behavior: 'smooth'
		  });
    });
});

/*Botón ir arriba----------------*/





function analiza_ancho_para_chat(){
	

    var ancho_nav = $(window).innerWidth();

    if(ancho_nav < 768 )
    {
        alert("no va chat");
    }
    else
    {
        alert("SI chat");
	

    }

}



function lanzoModalTempral(){
    var modal = $("#modalGuardaCotiza");
	modal.modal("show"); 
}


















function toggleClass(id) {
    // toggle class
	let boton = document.querySelector("[data-idprod='"+id+"']");
	//console.log(boton);
    boton.classList.toggle('activo');
  }



function activa_metodos_favoritos(){

	const botones_agregar_favoritos = document.querySelectorAll('.bt_agregar_favoritos');

	botones_agregar_favoritos.forEach(el => el.addEventListener('click', e => {
		e.preventDefault();

		// console.log(e.target.tagName); return false;
		// console.log(e.target.tagName); return false;
		//console.log(e.target.parentElement.getAttribute('data-nombre')); return false;

		var datos = "";

		if(e.target.tagName == "IMG") //Tuve que agregar esto porque el icono del corazon esta como imagen y no lo tomaba como parte del "a" para sacar los datos
		{
			var datos = {
				//Info GALLO:
				idProd: e.target.parentElement.getAttribute('data-idProd'),
				nombre: e.target.parentElement.getAttribute('data-nombre'),
				imagenProd: e.target.parentElement.getAttribute('data-imagenProd'),
				codigo: e.target.parentElement.getAttribute('data-codigo'),
				coleccion: e.target.parentElement.getAttribute('data-cole'),
				urlProd: e.target.parentElement.getAttribute('data-urlProd'),
				//url: document.location.href
			};
		}
		else
		{
			// leemos los datos clave del producto y los guardamos en un objeto
			var datos = {
				//Info GALLO:
				idProd: e.target.getAttribute('data-idProd'),
				nombre: e.target.getAttribute('data-nombre'),
				imagenProd: e.target.getAttribute('data-imagenProd'),
				codigo: e.target.getAttribute('data-codigo'),
				coleccion: e.target.getAttribute('data-cole'),
				urlProd: e.target.getAttribute('data-urlProd'),
				//url: document.location.href
			};
		}
		

		//console.log(`ID: ${datos.idProd},Codigo: ${datos.codigo}, Nombre: ${datos.nombre},URL PROD: ${datos.urlProd}, Imagen: ${datos.imagenProd}, favoritos:${JSON.stringify(favoritos)}`); 	return false;


		// leemos los favoritos del localStorage
		var favoritos = localStorage.getItem("favoritos") || "[]";
		favoritos = JSON.parse(favoritos);

		// buscamos el producto en la lista de favoritos
		var posLista = favoritos.findIndex(function(e) { return e.idProd == datos.idProd; });

		//console.log(`ID De prod en boton: ${e.target.getAttribute('data-idProd')}`); 


		if (posLista > -1) {
			// si está, lo quitamos
			favoritos.splice(posLista, 1);

			// console.log("eliminado");
		} else {
			// si no está, lo añadimos
			favoritos.push(datos);

			// console.log("agregado");
		}

		//console.log(`ID: ${datos.idProd},Codigo: ${datos.codigo}, Nombre: ${datos.nombre},URL PROD: ${datos.urlProd}, Imagen: ${datos.imagenProd}, favoritos:${JSON.stringify(favoritos)}`);
		//return false;

		// guardamos la lista actualizada de favoritos 
		localStorage.setItem("favoritos", JSON.stringify(favoritos));


		//Funcion para armar el coteo en el corazon:
		conteoFavoritos(favoritos.length);

		

		var idProdClick = "";
		
		if(e.target.tagName == "IMG") //Tuve que agregar esto porque el icono del corazon esta como imagen y no lo tomaba como parte del "a" para sacar los datos
		{
			// Me qeudo con el ID del producto que figura e el boton
			var idProdClick = e.target.parentElement.getAttribute('data-idProd');
		}
		else
		{
			// Me qeudo con el ID del producto que figura e el boton
			var idProdClick = e.target.getAttribute('data-idProd');
		}

		
		//console.log(`ID De prod en boton: ${idProdClick}`);  return false;
		
		
		// Vuelvo a levantar los favoritos para saber si el que estaba eliminando era el último:
		favoritos = localStorage.getItem("favoritos") || "[]";
		favoritos = JSON.parse(favoritos);


		if(favoritos.length == 0) //Si ya no hay favoritos paso el ID del ultmo producto eliminado para poder sacar la clase activo
		{
			asignoClaseActivoABotonesFavoritos(idProdClick);
		}
		else
		{
			asignoClaseActivoABotonesFavoritos();
		}


	}));


	// SINO HACE CLICK LEVANTO favoritos PARA VER SI EXISTE UN FAV Y PONER LA CLASE DE ACTIVO AL BOTON CORRESPONDIENTE 
	var favoritos = localStorage.getItem("favoritos") || "[]";
	favoritos = JSON.parse(favoritos);
	
	//Funcion para armar el coteo en el corazon
	conteoFavoritos(favoritos.length);
	//Funcion para asignar calse
	asignoClaseActivoABotonesFavoritos();

}




function conteoFavoritos(cantidadFav){
	const avisoFav = document.querySelector("#cantidad_favoritos");
	const corazon = document.querySelector("#icono_fav_header");

	if(cantidadFav > 0)
	{
		corazon.classList.add("activo");
		avisoFav.classList.add("activo");
		avisoFav.innerHTML = cantidadFav;
	}
	else
	{
		corazon.classList.remove("activo");
		avisoFav.classList.remove("activo");
		avisoFav.innerHTML = 0;
	}
}





function asignoClaseActivoABotonesFavoritos(idUltimoProdEliminado){

	// alert("Asigno clases"); 

	//console.log("Asigno clases"); 
	//console.log(idUltimoProdEliminado); 

	//Si no está definido quiere decir que no es el ultimo prod a eliminar
	if(typeof idUltimoProdEliminado === "undefined") 
	{	
		var favoritos = localStorage.getItem("favoritos") || "[]";
		favoritos = JSON.parse(favoritos);

		favoritos.forEach(el => {
		
			let idProdEnFav = el.idProd;

			const botones_agregar_favoritos = document.querySelectorAll('.bt_agregar_favoritos');
			const botones_agregar_favoritos_multiple = document.querySelectorAll('.favoritos_multiple_nuevo');
			
			//console.log(botones_agregar_favoritos_multiple); 


			
			botones_agregar_favoritos_multiple.forEach(boton => {
				let dataIdProdBoton = boton.dataset.idprod ;
				//console.log(`ID boton:${dataIdProdBoton}, ID Favoritos:${idProdEnFav}`);
				//Si el ID del producto que esta en fav es igual al ID del producto del data-idProd del boton, agrego la calse
				if( dataIdProdBoton == idProdEnFav )
				{
					//console.log("Data lugar:"+boton.dataset.lugar);
					//console.log("------------------- Va a agregar la clase activo del ID:"+dataIdProdBoton);
					boton.classList.add("activo");
					boton.innerHTML = ``; //Esto evita que inyecte el HTML que dice "No cotizar"
				}
				
			});  





			//Por cada fvorito que haya en el localStorage, recorro todos los botones para ponerle la clase
			botones_agregar_favoritos.forEach(el => {

				let dataIdProdBoton = el.dataset.idprod;

				//Si el ID del producto que esta en fav es igual al ID del producto del data-idProd del boton, agrego la calse
				if( dataIdProdBoton == idProdEnFav )
				{
					if( !(el.dataset.lugar == "multiple_colecciones") ) //Para que no inyecte HTML
					{
						el.classList.add("activo");
						el.innerHTML = `<img src="front/images/icons/icon-fav-relleno-amarillo.svg" class="iconos_botones_acciones"/> No Cotizar</a>`;
					}
				}
				else
				{
					if( !(el.dataset.lugar == "multiple_colecciones")) //Para que no inyecte HTML
					{
						el.classList.remove("activo");
						el.innerHTML = `<img src="front/images/icons/icon-fav-amarillo.svg" class="iconos_botones_acciones"/> Cotizar</a>`;	
					}
				}
			});

		});

	}
	else //Si ESTÁ definido quiere decir que es el ULTIMO prod a eliminar
	{
		const botones_agregar_favoritos = document.querySelectorAll('.bt_agregar_favoritos');

		//Recorro todos los botones para sacar la clase de ese especificamente (Porque puede estar en 2 lugares)
		botones_agregar_favoritos.forEach(el => {

			let dataIdProdBoton = el.dataset.idprod;

			//Si el ID del producto que esta en fav es igual al ID del producto de l data-idProd del boton, agrego la calse
			if(dataIdProdBoton == idUltimoProdEliminado)
			{
				//el.classList.remove("activo");
				
				if( !(el.dataset.lugar == "multiple_colecciones") ) //Para que no inyecte HTML
				{
					el.classList.add("activo");
					el.innerHTML = `<img src="front/images/icons/icon-fav-amarillo.svg" class="iconos_botones_acciones"/> Cotizar</a>`;
				}

			}
		});
	}

}








/*
document.getElementById("agregar-favoritos").addEventListener("click", function(e) {

// hacemos que no se ejecute el enlace
e.preventDefault();

// leemos los datos clave del producto y los guardamos en un objeto
var datos = {
    //id: document.getElementById("producto-id").value,
    //nombre: document.getElementById("producto-nombre").textContent,
    //url: document.location.href

    //Info GALLO:
    id: e.target.getAttribute('data-idProd'),
    nombre: e.target.getAttribute('data-nombre'),
    imagenProd: e.target.getAttribute('data-codigo="{{presentacion.id}}" data-imagenProd'),
    coleccion: e.target.getAttribute('data-cole'),
    url: document.location.href
};

// leemos los favoritos del localStorage
var favoritos = localStorage.getItem("favoritos") || "[]";
favoritos = JSON.parse(favoritos);

// buscamos el producto en la lista de favoritos
var posLista = favoritos.findIndex(function(e) { return e.id == datos.id; });

if (posLista > -1) {
    // si está, lo quitamos
    favoritos.splice(posLista, 1);
    e.target.classList.remove("activo");
    e.target.innerHTML = "Agregar a favoritos";
} else {
    // si no está, lo añadimos
    favoritos.push(datos);
    e.target.classList.add("activo");
    e.target.innerHTML = "Quitar de favoritos";
}

console.log(`ID: ${datos.id},Nombre: ${datos.nombre},URL: ${datos.url},Imagen: ${datos.imagenProd}, favoritos:${JSON.stringify(favoritos)}`);
//return false;

// guardamos la lista de favoritos 
localStorage.setItem("favoritos", JSON.stringify(favoritos));

});
*/
        
