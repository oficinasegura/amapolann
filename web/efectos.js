$(window).on('load', function(){
    alto_botonera_ppal_izq();
});


$(window).resize(function(){
    alto_botonera_ppal_izq();
});




/*Llamar a funcion cuando se termina de redimencionar la ventana*/
var id;
$(window).resize(function()
{
    clearTimeout(id);
    id = setTimeout(doneResizing, 10);
});

function doneResizing()
{
     alto_botonera_ppal_izq();
}
/*Llamar a funcion cuando se termina de redimencionar la ventana*/









function alto_botonera_ppal_izq(){

    var ancho_nav = $(window).width();
    var alto_nav = $(window).height();
    var alto_desarrollo_derecho = $('#contenedor_desarrollo').height();
    var alto_footer = $('.pie_abajo').height();
    var alto_header = $('#main-header').height();

    // Ancho máximo
    if(ancho_nav > 750)
    {
        $('#despleglable_botonera_ppal_izq').slideDown({duration: 600, easing: "easeOutExpo"}); //Bajo el desplegable por si lo cerro con JS

        if(alto_desarrollo_derecho < alto_nav)
        {
        	$('#contenedor_botonera_izq').css('min-height',alto_nav-alto_footer-alto_header);
        }
        else
        {
        	$('#contenedor_botonera_izq').css('min-height',alto_desarrollo_derecho+50);
        }
    }
    else
    {
        //$('#despleglable_botonera_ppal_izq').slideUp(); //Bajo el desplegable por si lo cerro con JS
        $('#contenedor_botonera_izq').css('min-height','auto');
    }

}



$('a.bt_despliega_botonera_ppal_izq').on('click', function(event){
    event.preventDefault();
    $('#despleglable_botonera_ppal_izq').slideToggle({duration: 600, easing: "easeOutExpo"});
    $('a.bt_despliega_botonera_ppal_izq').toggleClass('activo');
    $('#contenedor_botonera_izq').css({position: 'relative', top: '0px'});
});


$('ul.botonera_check li a').on('click', function(event){
	event.preventDefault();
	$(this).toggleClass('activo');
});















/*Menú desplegable mobile*/
$(document).on("ready", function(){
    $("a.ejemplo_cruz").on("click", function(event){
        event.preventDefault();
        $(this).toggleClass("animo_menu_hamburguesa");
        var alto_vh = $(window).height();
        // alert(alto_vh);
        $("#contiene_botonera_desplegable").css("height", alto_vh);
        $("#contiene_botonera_desplegable").slideToggle({duration: 600, easing: "easeOutExpo"});
    });
});
/*Menú desplegable mobile*/