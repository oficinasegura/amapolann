<?php
error_reporting(E_ERROR | E_PARSE);
session_start();
 include_once 'parameters.php';
 ini_set("memory_limit","200M"); 
  $db = new mysqli($database_host.":".$database_port, $database_user, $database_password,$database_name);
 
  
  $db->query("insert into ejecucion_cron (fecha,tipo) values (now(),'Enviar Alertas propiedades');");
  
  $id = 0;
 
 $rows = array();
 
 //$db->query("delete from propiedades_imagenes");
 
$sql = "select * from propiedades where fase_de_venta = 'Disponible'
and (actualizacion_expensas is null or actualizacion_expensas <= CURRENT_DATE() - INTERVAL 1 MONTH)
and id not in (select id_propiedad from propiedades_envios_wa where 
fecha_envio >= (CURRENT_DATE() - INTERVAL 1 MONTH));";

$squery = $db->query($sql);
while ($propiedad = $squery->fetch_assoc()) {
       
        $email_propietario = $propiedad["email_propietario"];
        $email_asesor = $propiedad["email_asesor"];
        $tipo_propiedad = $propiedad["tipo"];
        $tipo_operacion = $propiedad["operacion"];
        $nombre_completo_propietario = $propiedad["nombre_completo_propietario"];
        $desc_barrio = $propiedad["desc_barrio"];
        $nombre_propietario = $propiedad["nombre_propietario"];
        $pais = $propiedad["desc_pais"];
        $id = $propiedad["id"];
       
        $subject = "$nombre_completo_propietario - $tipo_propiedad - $tipo_operacion - $desc_barrio";
        
        $headers='From: Mas Propiedades .Net'.' <info@maspropiedades.net>' . "\r\n";
        if (strtoupper($pais) == "PARAGUAY"){
            $headers.='Reply-To: infopy@maspropiedades.net' . "\r\n";
        }
        else{
            $headers.='Reply-To: info@maspropiedades.net' . "\r\n";
        }
        //$to='emilianoross649@gmail.com';
        //$cc='jrossi.adp@gmail.com';
        $to= $email_propietario;
        $cc= $email_asesor;
        
        $from="info@maspropiedades.net";
          
        $message = "Hola $nombre_propietario,
                    <br><br>
                    Este es un correo que enviamos mensualmente para mantener la informacion de tu propiedad actualizada.
                    <br><br>
                    Es de suma importancia que respondas este correo con la informacion actual.
                    <br><br>
                    Según la ultima actualización en nuestro sistema los valores y detalles son:
                    <br>
                    https://www.maspropiedades.net/propiedad/$id
                    <br><br>   
                    Es correcto? Se ha modificado el valor de expensas o el monto pretendido?
                    <br>   
                    En los casos de venta indicar si acepta financiación  <b>  indicar modalidad por ejemplo: 50% + 10 cuotas  </b> 
                    <br><br>
                     <b> ¿Queres aumentar la posibilidad de venta? </b> 
                    <br>    
                    Para lograrlo es importante que el valor de publicacion este adecuado a los  <b>  valores del mercado. </b> 
                    <br>    
                    Para ello te ofrecemos realizar una  <b> tasacion virtual </b>  de tu propiedad y sugerirte un valor que aumente las
                    <br>
                    posibilidades de concretar la operacion
                    <br>
                    Solo debes responder este correo o contactarte por whatsapp.
                    <br><br>
                    Aguardamos tu respuesta.
                    <br><br>        
                    Saludos,
                    <br>
                    P.D.: Te invitamos a que conozcas nuestro nuevo portal en www.maspropiedades.net y el buscador en www.maspropiedades.net/buscador
                    <br><br>
                    Mas Propiedades .Net 
                    <br><br>
                    Buenos Aires-Argentina
                    <br>+54 11 5246 1980
                    <br>info@maspropiedades.net
                    <br><br>
                    Asuncion-Paraguay
                    <br>+595 985 457344
                    <br>infopy@maspropiedades.net
                    
                    <br><br>
                    www.maspropiedades.net
                    <br>Seguinos en <a href='https://www.facebook.com/www.maspropiedades.net'>Facebook</a>
                    <br>Seguinos en <a href='https://twitter.com/PropiedadesNet'>Twitter</a> 
                    <br>Seguinos en <a href='https://instagram.com/maspropiedades.net_ok'>Instagram</a>                                 
                     ";
        $uid = md5(uniqid(time()));        
        

         $body = array(
        "to" => $to, 
        "asunto" => $subject, 
        "mensaje" => $message, 
        "headers" => $headers,
        "from" => $from,    
        "cc" => $cc, 
        
        );
        
        
        //var_dump($headers);die;
        $opts = array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $body
        );
        $CURL_OPTS = array(         
        CURLOPT_USERAGENT => "MELI-PHP-SDK-2.0.0", 
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_CONNECTTIMEOUT => 10, 
        CURLOPT_RETURNTRANSFER => 1, 
        CURLOPT_TIMEOUT => 60
        );

        $ch = curl_init("http://c1391692.ferozo.com/index6.php");
        curl_setopt_array($ch, $CURL_OPTS);

        if(!empty($opts))
            curl_setopt_array($ch, $opts);


        $rta = curl_exec($ch);

        curl_close($ch);  
        var_dump($rta);
        var_dump("PROPIEDAD ENVIADA CON EXITO");
        sleep(1);
}
        
 