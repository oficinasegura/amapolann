<?php

namespace Serinco\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use \Doctrine\Common\Util\Debug;
use Serinco\CoreBundle\Entity;

class ExceptionController extends Controller
{
    
    public function showExceptionAction()
    {  
        
        $view = $this->renderView('MasPropiedadesCoreBundle:Default:tasaciones.html.twig', array(
                'user'=>$user,
                'videos'=>$videos,
                'tasaciones'=>$tasaciones
        ));


        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response;            
             
       
    }
    
    

   


}

