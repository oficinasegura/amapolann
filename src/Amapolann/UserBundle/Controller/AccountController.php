<?php

namespace Amapolann\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManagerInterface;
use Amapolann\UserBundle\Entity;
use Symfony\Component\HttpFoundation\Request;
/**
 * Account controller.
 *
 */
class AccountController extends Controller
{
    
     public function preventInjection($sql){
        
        return str_replace(";", "", $sql);
        
    }
    
    public function logAction()
    {
        
        $um = $this->get('user_manager');
        
        if ($_POST)
        {
            $username = $_POST["usuario"];
            $password= $_POST["pwd"];
        }
        else if (isset($_GET["usuario"]))
        {
            $username = $_GET["usuario"];
            $password= $_GET["pwd"];
        }
       
        $user = $um->authenticate($username,$password);
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm',array('error' => '1')));
        else{
            $_SESSION["usuario"] = $user["id"];
            //var_dump($user);
            return $this->redirect($this->generateUrl('grillaproductos'));
        }
    }

    public function recuperarAction()
    {
        $json = json_encode(['ok' => 'ok']);
        $response = new Response($json);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache');
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization');

        if (isset($_GET['usernameOrEmail'])) {

            $usernameOrEmail = $_GET['usernameOrEmail'];

            if (!empty($usernameOrEmail) && !empty($usernameOrEmail)) {
                if (strrpos($usernameOrEmail, '@'))
                    $query = 'SELECT * FROM usuario WHERE email = :usernameOrEmail AND habilitado = 1';
                else
                    $query = 'SELECT * FROM usuario WHERE nombre = :usernameOrEmail AND habilitado = 1';

                $conn = $this->getDoctrine()->getConnection();
                $stmt = $conn->prepare($this->preventInjection($query));
                $stmt->execute([
                    'usernameOrEmail' => $usernameOrEmail
                ]);

                $results = $stmt->fetchAll();

                if ($results && count($results) > 0) {
                    $descifrado = openssl_decrypt($results[0]['contrasena'], 'AES-128-CBC', '8d11d7c9b5ee156af537c2fdd63adf40', 0, '8d11d7c9b5ee156a');

                    $header = "From: info@amapolann.com\nReply-To:info@amapolann.com\n";
                    $header .= "Mime-Version: 1.0\n";
                    $header .= "Content-Type: text/plain; charset=UTF-8\n";
                    $mail = mail($results[0]['email'], 'Recuperación de contraseña', 'Tu contraseña es: ' . $descifrado, $header);
                    
                    $json = json_encode(['ok' => $results[0]['email']]);
                    $response->setContent($json);
                    $response->setStatusCode(200);
                } else {
                    $response->setStatusCode(404);
                }
            } else {
                $response->setStatusCode(404);
            }
        } else {
            $response->setStatusCode(404);
        }

        return $response;
    }

    public function girarfotoAction($id,$tipo="i")
    {
        
        $user = "unused";
        
        if (isset($_GET["tipo"]))
            $tipo = $_GET["tipo"];

        
        $conn = $this->getDoctrine()->getConnection();      
        
        
        $sql = "select imagen from productos_imagenes where id ='$id'";   
               
        $path = $conn->fetchColumn($this->preventInjection($sql));
        
        $directory ="C:\wamp64\www\serinco\web\\";
        $directory ="/var/www/"; 
        $file_dimensions = getimagesize($directory.$path);
        $ext = strtolower($file_dimensions['mime']);
        $ext_final = "jpg";
        switch($ext) {
            case 'image/png':
              $img = imagecreatefrompng($directory.$path);
              $ext_final = "png";
              break;
            case 'image/gif':
              $img = imagecreatefromgif($directory.$path);
              $ext_final = "gif";
              break;
            case 'image/jpeg':
              $img = imagecreatefromjpeg($directory.$path);
              $ext_final = "jpg";
              break;
            case 'image/bmp':
              $img = imagecreatefrombmp($directory.$path);
              $ext_final = "bmp";
              break;
            default:
              $img = null; 
            
            return $img;
          }
       
        // Rotate
        if ($tipo=="i")
            $rotate = imagerotate($img, 90, 0);
        else
            $rotate = imagerotate($img, -90, 0);

        $rand = rand(1, 10000);
        
        $path = str_replace(".$ext_final", $rand.".$ext_final", $path);
        //and save it on your server...
        
        imagejpeg($rotate, $directory.$path);
        
        $conn->exec($this->preventInjection("update productos_imagenes set imagen='$path' where id ='$id'"));
        
        $id_producto = $conn->fetchColumn($this->preventInjection("select id_producto from productos_imagenes where id ='$id'"));
        
        return $this->redirect($this->generateUrl('imagenesproductos', array('id'=>$id_producto)));
    }
    
    


    public function imprimirproductoAction(){
        //$id = $_POST["id_prod"];
    
        $id = "15661";
        
        $conn = $this->getDoctrine()->getConnection(); 
        
        $sql="SELECT * FROM productos_view WHERE id='15661'";
        $producto=$conn->fetchAll($this->preventInjection($sql));
        
        $nombre=$producto[0]["titulo"];
        $categoria=$producto[0]["categoria"];
        $coleccion=$producto[0]["coleccion"];
        $descripcion=$producto[0]["descripcion"];
        $imagen_portada=$producto[0]["imagen_portada"];
        $imagen_plano=$producto[0]["imagen_plano"];
        
        
        
        $hmtl='<html>
                            <head>
                              <title>Amapolann -  Griferias con estilo</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                            </head>
                            <body>
                               <div style="background-image: url(front/pdf/bg-pdf.jpg); width:100%; height:auto;">
                               
                                    

                                    <div style="width:640px; height:auto; padding:20px; margin:10px; border:1px solid #cc9635; ">
                                        <img src="front/pdf/head-pdf.jpg" style="height: auto;  width:100%; float:left; clear:left;">
                                        <h1 style="margin-bottom: 20px;">'.$nombre.'-'.$coleccion.'-'.$categoria.'</h1>
                                        <p style="font-size:13px; margin-bottom: 20px; font-weight:300; font-family:helvetica,sans serif;" >'.$descripcion.'</p>
                                        <img  src="'.$imagen_portada.'" style="width:200px; height:auto; float:left; clear:left;margin:10px"  />
                                        <img  src="'.$imagen_plano.'" style="width:200px; height:auto; float:left; clear:left;margin:10px" />
                                        <img  src="'.$imagen_portada.'" style="width:200px; height:auto;float:left; clear:left;margin:10px"  />

                                        <div> <p style="font-size:10px; margin-bottom: 20px; font-weight:300; font-family:helvetica,sans serif;" > Av. Cabildo 476 4°B -cp 1429 - Buenos Aires - Argentina
                                        (R) Amapolann - Marca registrada info@amapolann.com.ar</p> </div>
                                    </div>

                                  
                                </div>
                            </body>
                </html>';
        
        $random = rand(1, 999999);
        
        $nombre = "producto_".$id."_".$random.".pdf";
        
        $this->doPDF($nombre, $hmtl);  
        
        echo("ok");die;
    }
   
    
  public function doPDF($path='',$content='',$body=false,$style='',$mode=false,$paper_1='a4',$paper_2='portrait',$nro_acm="")
  {     


    
    $ruta = $this->container->getParameter('vendor');
    
    $ruta2 =  $this->container->getParameter('path_files');
    require_once $ruta.'/dompdf/include/autoload.inc.php';

    if( $body!=true and $body!=false ) $body=false; 
    if( $mode!=true and $mode!=false ) $mode=false; 
    if( $content!='' ) 
    {         
      //Las opciones del papel del PDF. Si no existen se asignan las siguientes:[*] 
      if( $paper_1=='' ) $paper_1='a4'; 
      if( $paper_2=='' ) $paper_2='portrait'; 
           
      //require_once '/../vendor/dompdf/include/dompdf.cls.php';
      $dompdf = new \Dompdf();
      $dompdf -> set_paper($paper_1,$paper_2); 
      $dompdf -> load_html(($content)); 
      //ini_set("memory_limit","32M"); //opcional  
      
      $dompdf -> render(); 
       
      //Creando  pdf 
      if($mode==false){
          //var_dump("ok");DIE;
          $dompdf->stream($path); 
          
      }
          
          
      
           
      //Lo guardo en un directorio y lo muestro 
      if($mode==true){
        if(file_put_contents($ruta2."/productos/pdfs/".$path, $dompdf->output()))
          return array("status" => true, "data" => $path); 
      } 
      
     
    } 
  } 
  
  
  
   public function modificarordencoleccionesAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_GET["id"];
        $vis = $_GET["vis"];
        
         
        $sql = "UPDATE colecciones set 
                orden= IFNULL(NULLIF('$vis',''),orden)
                where id = '$id'
        ";    

        $conn->exec($this->preventInjection($sql));
            
        echo("ok");
            
        die;
    }
  
   public function modificarordenAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_GET["id"];
        $vis = $_GET["vis"];
        
         
        $sql = "UPDATE productos set 
                orden= IFNULL(NULLIF('$vis',''),orden)
                where id = '$id'
        ";    

        $conn->exec($this->preventInjection($sql));
            
        echo("ok");
            
        die;
    }
    
     public function modificargaleriaAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_GET["id"];
        $vis = $_GET["vis"];
        
         
        $sql = "UPDATE productos set 
                mostrar_galeria= IFNULL(NULLIF('$vis',''),mostrar_galeria)
                where id = '$id'
        ";    

        $conn->exec($this->preventInjection($sql));
            
        echo("ok");
            
        die;
    }
    
    public function modificarvisibilidadAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_GET["id"];
        $vis = $_GET["vis"];
        
         
        $sql = "UPDATE productos set 
                activo= IFNULL(NULLIF('$vis',''),activo)
                where id = '$id'
        ";    

        $conn->exec($this->preventInjection($sql));
            
        echo("ok");
            
        die;
    }

    public function modificarvisibilidadImgadAction()
    {

        
        $id = $_GET["id"];
        $id_p = $_GET["vis"];
        
        $um = $this->get('user_manager');
        
       $user = $um->getLoggedInUser();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
       

        $conn = $this->getDoctrine()->getConnection();
        
        $sql = "update productos_imagenes set mostrar_galeria = '$id_p' where id='$id';";
      
        $conn->exec($this->preventInjection($sql));
        
     
        
        return $this->redirect($this->generateUrl('imagenesproductos', array('id'=>$id_p)));
    }
    

    
    
    
    public function modificarvisibilidadcoleccionesAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_GET["id"];
        $vis = $_GET["vis"];
        
         
        $sql = "UPDATE colecciones set 
                activo= IFNULL(NULLIF('$vis',''),activo)
                where id = '$id'
        ";    

        $conn->exec($this->preventInjection($sql));
            
        echo("ok");
            
        die;
    }
    
    public function modificarmultiplesAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $ids = $_POST["ids"];
        
        $arr = explode(",", $ids);
        
        
        $activo = (isset($_POST["activo"])) ? $_POST["activo"] : "";
        $mostrar  = (isset($_POST["mostrar"])) ? $_POST["mostrar"] : "";
        
        foreach ($arr as $key) {
            $arrin = explode("_", $key);
            $id = $arrin[1];
            
            $sql = "UPDATE productos set 
                    activo= IFNULL(NULLIF('$activo',''),activo),
                    mostrar_galeria = IFNULL(NULLIF('$mostrar',''),mostrar_galeria)
                    where id = '$id'
            ";    
            
            $conn->exec($this->preventInjection($sql));
            
        }
            
        die;
    }

    public function modificarmultiplesImgadAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $ids = $_POST["ids"];
        
        $arr = explode(",", $ids);
        
        
        $activo = (isset($_POST["activo"])) ? $_POST["activo"] : "";
        $mostrar  = (isset($_POST["mostrar"])) ? $_POST["mostrar"] : "";
        
        foreach ($arr as $key) {
            $arrin = explode("_", $key);
            $id = $arrin[1];
            
            $sql = "UPDATE productos_imagenes set 
                    mostrar_galeria= IFNULL(NULLIF('$activo',''),activo),
                    mostrar_galeria = IFNULL(NULLIF('$mostrar',''),mostrar_galeria)
                    where id = '$id'
            ";    
            
            $conn->exec($this->preventInjection($sql));
            
        }
            
        die;
    }
    
    public function modificarmultiplescoleccionesAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $ids = $_POST["ids"];
        
        $arr = explode(",", $ids);
        
        $activo = $_POST["activo"];
       
        foreach ($arr as $key) {
            $arrin = explode("_", $key);
            $id = $arrin[1];
            
            $sql = "UPDATE colecciones set activo = '$activo' where id = '$id'";
            
            $conn->exec($this->preventInjection($sql));
            
        }
            
        die;
    }
    
     public function grillaproductosAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "orden";
        $id_coleccion = "";
        
        $url='grillaproductos';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            if (isset($_GET["id_coleccion"])){
                $id_coleccion = $_GET["id_coleccion"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }
            
        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from productos_view WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from productos WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        if ($id_coleccion!=""){
            $sql.=" AND id_categoria = '$id_coleccion'";
            $sql_count.=" AND id_categoria = '$id_coleccion'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        //Colecciones para buscador
        
        $sql="SELECT * FROM colecciones WHERE 1=1";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:productos.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'id_coleccion'=>$id_coleccion,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'colecciones'=>$colecciones,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    

   
    public function productosfrontAction()
    {
            
            $conn = $this->getDoctrine()->getConnection();
            
            $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
            $colecciones=$conn->fetchAll($this->preventInjection($sql));
            $arr_colecciones = array();
            foreach ($colecciones as $coleccion) {
                $id = $coleccion["id"];
                $productos=$conn->fetchAll($this->preventInjection("SELECT * FROM productos where id_categoria = '$id'"));
                $coleccion["productos"] = $productos;
                $arr_colecciones[] = $coleccion;
            }

            $sql="SELECT * FROM terminaciones WHERE 1=1";
            $terminaciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
            $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
            
        $view = $this->renderView('AmapolannUserBundle:Account:productosfront.html.twig', array(
            'colecciones'=>$arr_colecciones,    
            'terminaciones'=>$terminaciones,
            'descrip_coleccion'=>$descrip_coleccion,
        
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function productofrontAction()
    {   
        if(isset($_GET["id"])){
            
            $id=$_GET["id"];
            
            $conn = $this->getDoctrine()->getConnection();
            
            $id_coleccion=$conn->fetchColumn($this->preventInjection("SELECT id_categoria FROM productos WHERE id='$id'"));
            
            $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
            $colecciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM terminaciones where id in(select id_terminacion from terminaciones_por_producto WHERE id_producto = '$id')";
            $terminaciones_x_prod=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM terminaciones WHERE 1=1";
            $terminaciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM productos_view WHERE id='$id';";
            $producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql= "select * from productos_imagenes where id_producto = '$id' order by orden;";
            $imagenes= $conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM productos_view WHERE id_categoria='$id_coleccion' AND id != '$id' AND activo=1 order by orden;";
            $productos_coleccion =$conn->fetchAll($this->preventInjection($sql));
            //var_dump($productos_coleccion);die;
            //Galeria de fotos
            $sql="SELECT * FROM productos_imagenes_view WHERE id_producto in (SELECT id FROM productos_view WHERE id_categoria='$id_coleccion' AND activo=1)";
            $galeria_productos=$conn->fetchAll($this->preventInjection($sql));
            
            
            $sql="SELECT * FROM levas where id in (SELECT id_leva from levas_por_coleccion_view WHERE id_coleccion='$id_coleccion')";
            $levas_x_coleccion=$conn->fetchAll($this->preventInjection($sql));
            }   
        if(!isset($_GET["id"]))
            return $this->redirect($this->generateUrl('productos', array()));
        
        $view = $this->renderView('AmapolannUserBundle:Account:productofront.html.twig', array(
            'id'=>$id,
            'producto'=>$producto,
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_x_prod'=>$terminaciones_x_prod,
            'imagenes'=>$imagenes,
            'productos_coleccion'=>$productos_coleccion,
            'galeria_productos'=>$galeria_productos,
            'levas_x_coleccion'=>$levas_x_coleccion,
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function galeriaproductoajaxAction()
    {   
        if(isset($_GET["id"])){
            
            $id=$_GET["id"];
            $pagina=intval($_GET["pagina"]);
            $offset = ($pagina - 1) * 6;
            $conn = $this->getDoctrine()->getConnection();
            
            $id_coleccion=$conn->fetchColumn($this->preventInjection("SELECT id_categoria FROM productos WHERE id='$id'"));
            
            $sql="SELECT * 
            FROM productos_imagenes_view 
            WHERE id_producto in 
            (SELECT id FROM productos_view WHERE id_categoria='$id_coleccion' AND activo=1) 
            LIMIT 6 OFFSET $offset";
            $galeria_productos=$conn->fetchAll($this->preventInjection($sql));
            
        }   
        if(!isset($_GET["id"]))
            return $this->redirect($this->generateUrl('productos', array()));
        
        $view = $this->renderView('AmapolannUserBundle:Account:galeriaajax.html.twig', array(
            'imagenes_galeria'=>$galeria_productos,
            'dataSlide' => $offset
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization');

        return $response; 
    }
    
    public function coleccionfrontAction()
    {   
        if(isset($_GET["id"])){
            
            $id=$_GET["id"];
            
            //echo $id;
            $conn = $this->getDoctrine()->getConnection();
            
            $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
            $colecciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM terminaciones WHERE 1=1";
            $terminaciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM colecciones WHERE id='$id';";
            $coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM productos_view WHERE id_categoria='$id' and activo='1' order by orden ASC;";
            $productos_coleccion=$conn->fetchAll($this->preventInjection($sql));

            
            $arr_productos = array();
            foreach ($productos_coleccion as $producto){
                    $id_prod = $producto["id"];
                    $imagenes=$conn->fetchAll($this->preventInjection("SELECT * FROM productos_imagenes_view where id_producto = '$id_prod'"));
                    $producto["imagenes"] = $imagenes; 
                    $arr_productos[]=$producto;
            }
            
            
            //Terminaciones productos
            
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_coleccion where id_coleccion = '$id');";
            $terminaciones_coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            //Img galeria ok
            
            $sql="SELECT * FROM productos_imagenes_view WHERE id_coleccion = '$id' and id_producto in(SELECT id from productos WHERE activo=1)";
            $imagenes_galeria=$conn->fetchAll($this->preventInjection($sql));

            $sql= "SELECT * FROM banner_cotizacion WHERE id = 1";
            $banner=$conn->fetchAll($this->preventInjection($sql));
            
        }
        if(!isset($_GET["id"]))
            return $this->redirect($this->generateUrl('productos', array()));
        
        $view = $this->renderView('AmapolannUserBundle:Account:coleccionfront.html.twig', array(
            'coleccion'=>$coleccion,
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'productos_coleccion'=>$arr_productos,
            'imagenes_galeria'=>$imagenes_galeria,
            'terminaciones_coleccion'=>$terminaciones_coleccion,
            'banner'=>$banner
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function galeriaajaxAction()
    {   
        if(isset($_GET["id"])){
            
            $id=$_GET["id"];
            $pagina=intval($_GET["pagina"]);
            $offset = ($pagina - 1) * 6;
            $conn = $this->getDoctrine()->getConnection();
            
            //Img galeria
            $sql="SELECT * 
            FROM productos_imagenes_view 
            WHERE id_coleccion = '$id' 
            and id_producto in(SELECT id from productos WHERE activo=1)
            LIMIT 6 OFFSET $offset";

            $imagenes_galeria=$conn->fetchAll($this->preventInjection($sql));

            
        }
        if(!isset($_GET["id"]))
            return $this->redirect($this->generateUrl('productos', array()));
        
        $view = $this->renderView('AmapolannUserBundle:Account:galeriaajax.html.twig', array(
            'imagenes_galeria'=>$imagenes_galeria,
            'dataSlide' => $offset
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET');
        $response->headers->set('Access-Control-Allow-Headers', 'Content-Type, Authorization');

        return $response; 
    }
    
    public function busquedaAction()
    {   
        if(isset($_GET["id"])){
            
            $id=$_GET["id"];
            
            $conn = $this->getDoctrine()->getConnection();
            
            $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
            $colecciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM terminaciones WHERE 1=1";
            $terminaciones=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM colecciones WHERE id='$id';";
            $coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM productos_view WHERE id_categoria='$id' and activo='1' order by orden ASC;";
            $productos_coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            $arr_productos = array();
            foreach ($productos_coleccion as $producto){
                    $id_prod = $producto["id"];
                    $imagenes=$conn->fetchAll($this->preventInjection("SELECT * FROM productos_imagenes_view where id_producto = '$id_prod'"));
                    $producto["imagenes"] = $imagenes; 
                    $arr_productos[]=$producto;
            }
            
            
            //Terminaciones productos
            
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_coleccion where id_coleccion = '$id');";
            $terminaciones_coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            //Img galeria
            
            $sql="SELECT * FROM productos_imagenes_view WHERE id_coleccion = '$id' and id_producto in(SELECT id from productos WHERE activo=1)";
            $imagenes_galeria=$conn->fetchAll($this->preventInjection($sql));
            
            //var_dump($arr_productos);die;
        }
        if(!isset($_GET["id"]))
            return $this->redirect($this->generateUrl('productos', array()));
        
        $view = $this->renderView('AmapolannUserBundle:Account:busqueda.html.twig', array(
            'coleccion'=>$coleccion,
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'productos_coleccion'=>$arr_productos,
            'imagenes_galeria'=>$imagenes_galeria,
            'terminaciones_coleccion'=>$terminaciones_coleccion,
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    



    public function nosotrosAction()
    {
        
        
        $view = $this->renderView('AmapolannUserBundle:Account:nosotros.html.twig', array(
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function empresaAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM terminaciones WHERE 1=1";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Productos'";
        $descrip_producto=$conn->fetchColumn($this->preventInjection($sql));
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion de la empresa'";
        $descrip_empresa=$conn->fetchColumn($this->preventInjection($sql));
        $sql="SELECT * FROM carrousel_empresa WHERE 1=1 ORDER BY orden ASC";
        $carrousel=$conn->fetchAll($this->preventInjection($sql));
        $sql="SELECT * FROM textos_predefinidos WHERE titulo='Separador Empresa'";
        $griferias_premium=$conn->fetchAll($this->preventInjection($sql));

        //Productos destacados
        $sql="SELECT * FROM productos_view WHERE activo=1 AND mostrar_galeria = 1 order by orden";
        $galeria_productos=$conn->fetchAll($this->preventInjection($sql));
        
        
        $view = $this->renderView('AmapolannUserBundle:Account:empresa.html.twig', array(
            'colecciones'=>$colecciones,
            'terminaciones'=>$terminaciones,
            'descrip_empresa'=>$descrip_empresa,
            'descrip_producto'=>$descrip_producto,
            'descrip_coleccion'=>$descrip_coleccion,
            'carrousel'=>$carrousel,
            'griferias_premium'=>$griferias_premium,
            'galeria_productos'=>$galeria_productos
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function textoAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = 0;
        
        
        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo_editable = (isset($_POST["titulo_editable"])) ? $_POST["titulo_editable"] : "";
            $subtitulo = (isset($_POST["subtitulo"])) ? $_POST["subtitulo"] : "";
            $texto = (isset($_POST["texto"])) ? $_POST["texto"] : "";
            $boton = (isset($_POST["boton"])) ? $_POST["boton"] : "";
            $link = (isset($_POST["link"])) ? $_POST["link"] : "";
           
            $sql = " UPDATE textos_predefinidos SET
                        titulo_editable =   '$titulo_editable',
                        subtitulo =   '$subtitulo',
                        texto =   '$texto',
                        link =   '$link',
                        boton =   '$boton'
                       "
                         . "WHERE `ID` = $id;
                            ";
                                
                 $conn->exec($this->preventInjection($sql));
                
                
                return $this->redirect($this->generateUrl('textos'));
            }
        
        
        $mensaje = null;
        if ($id>0){
            $sql= "select * from textos_predefinidos where id = '$id';";

            $texto = $conn->fetchAll($this->preventInjection($sql));
          
        }
        
       
        $view = $this->renderView('AmapolannUserBundle:Account:texto.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'texto'=>$texto
                
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    
    
    public function textosAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        $conn = $this->getDoctrine()->getConnection();
        $sql="SELECT * FROM textos_predefinidos WHERE 1=1";
        $textos=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:textos.html.twig', array(
            'textos'=>$textos,
            'user'=>$user,
           
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function contactpageAction()
    {
        
        $enviado=0;
        
        if(isset($_GET["enviado"])){
            $enviado=$_GET["enviado"];
        }
        
        $conn = $this->getDoctrine()->getConnection();
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM terminaciones WHERE 1=1";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='TerminosyCondiciones'";
        $terminos=$conn->fetchColumn($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:contactpage.html.twig', array(
            'colecciones'=>$colecciones,
            'terminaciones'=>$terminaciones,
            'descrip_coleccion'=>$descrip_coleccion,
            'terminos'=>$terminos,
            'enviado'=>$enviado,
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function contactoFavAction()
    {
        
        $enviado=0;
        
        if(isset($_GET["enviado"])){
            $enviado=$_GET["enviado"];
			return $this->redirect($this->generateUrl('ambientaciones'));
        }
        
		$conn = $this->getDoctrine()->getConnection();
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));

            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));

            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));

            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));

            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));

            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));

            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));

        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));

        $sql="SELECT * FROM ambientaciones";
        $ambientaciones=$conn->fetchAll($this->preventInjection($sql));


        $view = $this->renderView('AmapolannUserBundle:Account:ambientacionesfront.html.twig', array(
            'ambientaciones'=>$ambientaciones,
            'colecciones'=>$colecciones,
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
			'enviado'=>$enviado,

        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache');
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response;

    }
       
    
    public function contactpageabmAction()
    {
        $fromName=$_POST["nombre"];
        $from=$_POST["mail"];
        $telefono=$_POST["telefono"];
        $localidad=$_POST["localidad"];
        $descripcion=$_POST["descripcion"];
		
        
        $asunto="Formulario AMAPOLANN CLIENTE $fromName -- -- -- -- -";
        $mensaje='<html>
                            <head>
                              <title>Amapolann -  Griferias con estilo</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                            </head>
                            <h2>Contacto AMAPOLAN</h2>
                            
                            <body>
                                <br><br><p>__________________________________________</p>
                                <p style="font_size:10px !important;">Contacto AMAPOLANN</p><br>
                                <p style="font_size:10px !important;">Nombre:'.$fromName.'</p><br>
                                <p style="font_size:10px !important;">Mail:'.$from.'</p><br> 
                                <p style="font_size:10px !important;">Telefono:'.$telefono.'</p><br>    
                                <p style="font_size:10px !important;">Location:'.$localidad.'</p><br>
                                <p style="font_size:10px !important;">Fecha de cierre:</p><br>
                                <p style="font_size:10px !important;">Fecha de entrega requerida:</p><br>
                                <p style="font_size:10px !important;">Mensaje:'.$descripcion.'</p><br>

                                <p style="font_size:10px !important;">Cotizar:</p><br>
                                <p style="font_size:10px !important;">Como nos conocio?:</p><br>
                                <p style="font_size:10px !important;">Arquitecto:</p><br>
                                <p style="font_size:10px !important;">O. Sponsor:</p><br>
                                <p style="font_size:10px !important;">Rep::</p><br>
                                <p style="font_size:10px !important;">Competimos con::</p><br>
                            </body>
                </html>';
        $to="info@amapolann.com";
        //$to = "CUENTA_DESTINO@DOMINIO.COM";
        
        //contenido del mensaje   
        $contenido = "\n\n"."___________________________________"."\n\n";
        $contenido .= "Contacto AMAPOLANN"."\n\n";
        $contenido .= "Cliente: ".$fromName."\n\n";
        $contenido .= "Email: ".$from."\n\n";
        $contenido .= "Telefono: ".$telefono."\n\n";
        $contenido .= "Localidad: ".$localidad."\n\n";
        $contenido .= "Fecha de cierre:"."\n\n";
        $contenido .= "Fecha de entrega requerida:"."\n\n";
        $contenido .= "Mensaje: ".$descripcion."\n\n";
        $contenido .= "Cotizar"."\n\n";
        $contenido .= "Como nos conocio?:"."\n\n";
        $contenido .= "Arquitecto:"."\n\n";
        $contenido .= "O Sponsor:"."\n\n";
        $contenido .= "Rep:"."\n\n";
        $contenido .= "Competimos con:"."\n\n";
        //******************************************************************* */
        $header = "From: info@amapolann.com\nReply-To:info@amapolann.com\n";
        $header .= "Mime-Version: 1.0\n";
        $header .= "Content-Type: text/plain";

        /* Filtro anti spam */
        $expresion_regular = '/(@yahoo\.se|@sosalarm\.se|@cluedesign\.com\.au|@BIGPOND\.NET\.AU|@mail\.ru|@kanzalshamsprojectmgt\.com|no-reply|mikeEratawbava|mikeNusruisp|c6h12o6gal|alfredegov|mikevope|elouisefriloux|zelatcol|@uniglobetraveltimes\.com)/i';
        if (filter_var($from, FILTER_VALIDATE_EMAIL)) {
            if(!(preg_match($expresion_regular, $from))){
                /* reCAPTCHA v2 */
                if (!empty($_POST)) {
                    if (isset($_POST['g-recaptcha-response'])) {
                        $claveSecreta = '6LfBMYAoAAAAAAKbRq6CXKzu3AKfHvYCsSAsOb8p';
                        $captcha = $_POST['g-recaptcha-response'];
                        $respuesta = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$claveSecreta&response=$captcha");
                        $arrayRespuesta = json_decode($respuesta, true);
                        if (isset($arrayRespuesta['success']) && $arrayRespuesta['success']) {
                            $rta = mail($to, $asunto, $contenido, $header);
                        }
                    }
                }
            }
        }
        //$this->enviarMail($to,$asunto,$from,$fromName,$mensaje);
        
    
        return $this->redirect($this->generateUrl('contactpage', array('enviado'=>1)));
    }

    public function contactoFavabmAction()
    {
		$fromName=$_POST["nombre"];
        $from=$_POST["mail"];
        $telefono=$_POST["telefono"];
        $localidad=$_POST["localidad"];
        $descripcion=$_POST["descripcion"];
		$descripcion2=$_POST["descripcion2"]; //solo para el asunto
		
        $asunto="Consulta de Precios ANN - $fromName ( $localidad ) - $telefono - $descripcion2";
        $mensaje='<html>
                            <head>
                              <title>Amapolann -  Griferias con estilo</title>
                              <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
                            </head>
                            <h3><strong>Consulta precios - WebPage AMAPOLANN</strong></h3>
							
                            <body>
							<table border="0" width="600">	
							
                                <tr><td><p style="font_size:10px !important;">Nombre:</td><td>'.$fromName.'</td></p></tr>
                                <tr><td><p style="font_size:10px !important;">Mail:</td><td>'.$from.'</td></p></tr>
                                <tr><td><p style="font_size:10px !important;">Telefono:</td><td>'.$telefono.'</td></p></tr>
                                <tr><td><p style="font_size:10px !important;">Location:</td><td>'.$localidad.'</td></p></tr>
								<tr><td><p style="font_size:10px !important;">Fecha de cierre:</td><td></p><br></td></p></tr>
								<tr><td><p style="font_size:10px !important;">Fecha de entrega req:</td><td></p><br></td></p></tr>
								</table>
                                <p style="font_size:10px !important;">Les solicito cotizacion acerca de los siguientes productos:</p><br>
								
							
                              
                                <table border="0" width="600">
                                    <tbody>
                                        '.$descripcion.'
                                    </tbody>
                                </table>
                                
                                <p style="font_size:10px !important;">Compet:</p><br>
                                <p style="font_size:10px !important;">Fecha Instal:</p><br>
                                <p style="font_size:10px !important;">Sponsor:</p><br>
                                <p style="font_size:10px !important;">Rep ANN:</p><br>
                                <p style="font_size:10px !important;">Asingado a:</p><br>



                            </body>
                </html>';
		
        $to="info@amapolann.com".", "; // atención a la coma
        $to .= "carloscambas@amapolann.com"; //segundo destinatario
        //$to .= "damiangallo@gmail.com"; //segundo destinatario

        //contenido del mensaje
        $contenido = "Consulta de Precios - WebPage AMAPOLANN"."\n\n";
        $contenido .= "Cliente:      ".$fromName."\n\n";
        $contenido .= "Email:        ".$from."\n\n";
        $contenido .= "Telefono:   ".$telefono."\n\n";
        $contenido .= "Localidad:  ".$localidad."\n\n";
        $contenido .= "Fecha de cierre:"."\n\n";
        $contenido .= "Fecha de entrega requerida:"."\n\n";
        $contenido .= "Mensaje: ".$descripcion."\n\n\n";
		$contenido .= "Compet: \n\n";
		$contenido .= "Fecha Instal: \n\n";
		$contenido .= "Sponsor: \n\n";
		$contenido .= "Rep ANN: \n\n";
		$contenido .= "Asingado a: \n\n";
		
        //******************************************************************* */
		
		$headers  = "From: deptocomercial@amapolann.com\nReply-To:info@amapolann.com\n";
		//$headers .= "Reply-To: " . strip_tags($_POST['req-email']) . "\r\n";
		$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
	
        $header = "From: deptocomercial@amapolann.com\nReply-To:info@amapolann.com\n";
        $header .= "Mime-Version: 1.0\n";
        $header .= "Content-Type: text/html; charset=UTF-8\r\n";
        $rta = mail($to, $asunto, $mensaje, $header);
        //$this->enviarMail($to,$asunto,$from,$fromName,$mensaje);


        return $this->redirect($this->generateUrl('contactoFav', array('enviado'=>1)));
    }

    
    
    
    
    public function enviarMail($to,$asunto,$from,$fromName,$body,$servidor="c2230628.ferozo.com",$puerto="465",$ssl="ssl",$usuario="info@amapolann.com",$contraseña="Entrada9090"){
              // Create the Transport
        $transport = (new \Swift_SmtpTransport($servidor, $puerto, $ssl))
            ->setUsername($usuario)
            ->setPassword($contraseña)
            ;
        
          // Create the Mailer using your created Transport
          $mailer = new \Swift_Mailer($transport);

          // Create a message
          $message = (new \Swift_Message($asunto))
            ->setFrom([$from => $fromName])
            ->setTo([$to])
            ->setBody($body)
            ->setContentType("text/html")
            ;
          
          // Send the message
          $result = $mailer->send($message);
          //var_dump($result);die;
          
          return null;

    }

    public function distribuidoresfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT * FROM distribuidores ORDER BY orden ASC";
            $distribuidores=$conn->fetchAll($this->preventInjection($sql));

        
        $view = $this->renderView('AmapolannUserBundle:Account:distribuidoresfront.html.twig', array(
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
            'distribuidores'=>$distribuidores,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
   
    }
    
    public function terminacionesfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:terminacionesfront.html.twig', array(
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function ambientacionesfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT * FROM ambientaciones";
        $ambientaciones=$conn->fetchAll($this->preventInjection($sql));

        
        $view = $this->renderView('AmapolannUserBundle:Account:ambientacionesfront.html.twig', array(
            'ambientaciones'=>$ambientaciones,
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
   
    }

    public function resultadosbusquedaAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        $buscar="sss ";
        $busqueda="";

        if(isset($_POST["buscar"])){
            $busqueda=$_POST["buscar"];
        }



        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }

        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT * FROM ambientaciones";
        $ambientaciones=$conn->fetchAll($this->preventInjection($sql));


        $palabras = explode (' ', $busqueda);
        // var_dump ($palabras)  ;

        $qTitulo = "";
        $qColeccion = "";
        $qDescripcion = "";
		$qKeywords = "";

        // hacer consulta 
        for ($i = 0; $i < count($palabras); $i++)
        {
            //if (strlen($queryBusqueda) > 2 AND strlen($palabra2) > 2 strlen($palabra3) > 2  ) {
            //primera query
            
            $qTitulo      .= ($qTitulo      != "" ? " AND " : "") . "titulo      LIKE '%" . $palabras[$i] . "%'";
            $qColeccion   .= ($qColeccion   != "" ? " AND " : "") . "coleccion   LIKE '%" . $palabras[$i] . "%'";
            $qDescripcion .= ($qDescripcion != "" ? " AND " : "") . "descripcion LIKE '%" . $palabras[$i] . "%'";
			$qKeywords .= ($qKeywords != "" ? " AND " : "") . "keywords LIKE '%" . $palabras[$i] . "%'";
        }
        
        // busqueda niuvel 1 -> solo producto exacto
        $sql = "SELECT * FROM productos_view WHERE activo = 1 AND (($qTitulo) OR ($qColeccion) OR ($qDescripcion) OR ($qKeywords)) ORDER BY id";

        $galeria_productos = $conn->fetchAll($this->preventInjection($sql));

        //busqeda nivel 2 ---------------------------------------------------------------------------------
        
        // hacer consulta 
        for ($i = 0; $i < count($palabras); $i++)
        {
            //if (strlen($queryBusqueda) > 2 AND strlen($palabra2) > 2 strlen($palabra3) > 2  ) {
            //primera query
            
            $qTitulo      .= ($qTitulo      != "" ? " OR " : "") . "titulo      LIKE '%" . $palabras[$i] . "%'";
            $qColeccion   .= ($qColeccion   != "" ? " OR " : "") . "coleccion   LIKE '%" . $palabras[$i] . "%'";
            $qDescripcion .= ($qDescripcion != "" ? " OR " : "") . "descripcion LIKE '%" . $palabras[$i] . "%'";
			$qKeywords .= ($qKeywords != "" ? " OR " : "") . "keywords LIKE '%" . $palabras[$i] . "%'";
        }
        
        // busqueda niuvel 1 -> solo producto exacto
        $sql = "SELECT * FROM productos_view WHERE activo = 1 AND (($qTitulo) OR ($qColeccion) OR ($qDescripcion) OR ($qKeywords)) ORDER BY id";

      
    
        $galeria_productos2=$conn->fetchAll($this->preventInjection($sql));
        
    
 
        
     

        $view = $this->renderView('AmapolannUserBundle:Account:resultadosbusqueda.html.twig', array(
            'ambientaciones'=>$ambientaciones,
            'colecciones'=>$colecciones, 
            'buscar'=>$buscar,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'galeria_productos'=>$galeria_productos,
            'galeria_productos2'=>$galeria_productos2,
            'busqueda'=>$busqueda,
            'levas'=>$levas,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
   
    }




    public function combinacionesfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT * FROM combinaciones";
        $combinaciones=$conn->fetchAll($this->preventInjection($sql));

        
        $view = $this->renderView('AmapolannUserBundle:Account:combinacionesfront.html.twig', array(
            'combinaciones'=>$combinaciones,
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
   
    }


    public function ambientacionfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
       
        $sql="SELECT * FROM ambientaciones WHERE 1=1";
        $ambientaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:ambientacionfront.html.twig', array(
            'ambientaciones'=>$ambientaciones,    
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function combinacionfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
       
        $sql="SELECT * FROM combinaciones WHERE 1=1";
        $combinaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:combinacionfront.html.twig', array(
            'combinaciones'=>$combinaciones,    
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function resultadobusquedaAction()
        
            {
                $em = $this->getDoctrine()->getEntityManager();
              
                $conn = $this->getDoctrine()->getConnection();        
              
                $buscar=" sdfsdfsdfsdfsdfsdfsdf";
                //$buscar =$_GET["buscar"];
        /*
                if (isset($_GET["buscar"])){
                    $buscar = $_GET["buscar"];
                }
                
                if (isset($_POST["buscar"])){
                    $buscar = $_POST["buscar"];
                }
              
                if ($buscar!="buscars"){
                    $sql= "select 1 from productos where descripcion='$buscar' ;";
                    
                    
                $propiedad = null;
                if ($_POST){
                    
                    $buscar = (isset($_POST["buscar"])) ? $_POST["buscar"] : "";
                            
                    
                    $conn->exec($this->preventInjection($sql);
                    $propiedad2 = $conn->fetchAll($this->preventInjection($sql);
                                                
                       
                    return $this->redirect($this->generateUrl('resultadosbusqueda', array('busqueda'=>$busqueda)));
                        
                    }
                    
                    
                }
                
                  */
                  $propiedad2 = "sdsdsdsdsdsdsdsdsdsdsds";
         
                $view = $this->renderView('AmapolannUserBundle:Account:resultadosbusqueda.html.twig', array(

                        'propiedad2'=>$propiedad2,
                        'buscar'=>$buscar,
                ));
        
                $response = new Response($view);
        
                $response->headers->set('Cache-Control', 'public, max-age=0');
                $response->headers->set('Pragma', 'no-cache'); 
                $response->setMaxAge(0);
                $response->setSharedMaxAge(0);
                $response->headers->addCacheControlDirective('must-revalidate', true);
                $response->headers->addCacheControlDirective('proxy-revalidate', true);
        
                return $response; 
            }
    
        
    




    public function galeriagralfrontAction()
    {
        $conn = $this->getDoctrine()->getConnection();
       
        $sql="SELECT * FROM galeriagral WHERE 1=1";
        $galeriagrales=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:galeriagralfront.html.twig', array(
            'galeriagrales'=>$galeriagrales,    
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    

    public function homeAction()
    {
        $conn = $this->getDoctrine()->getConnection();
            
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM terminaciones WHERE 1=1";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));        
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Texto Banner'";
        $texto_banner=$conn->fetchColumn($this->preventInjection($sql));    
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Informacion adicional'";
        $info_adicional=$conn->fetchColumn($this->preventInjection($sql));    
        
        $sql="SELECT * FROM carrousel_imagenes WHERE 1=1 ORDER BY orden ASC";
        $carrousel=$conn->fetchAll($this->preventInjection($sql));
        
        //Productos destacados
        $sql="SELECT * FROM productos_view WHERE activo=1 AND mostrar_galeria = 1 order by orden";
        $galeria_productos=$conn->fetchAll($this->preventInjection($sql));
        
        //Slider productos destacados
        //$sql="SELECT * FROM productos_imagenes_view WHERE id_producto IN (SELECT id FROM productos WHERE mostrar_galeria=1 AND activo=1) order by id_producto;";
        //$fotos_destacados=$conn->fetchAll($this->preventInjection($sql);
        
        $view = $this->renderView('AmapolannUserBundle:Account:index.html.twig', array(
            'colecciones'=>$colecciones,
            'terminaciones'=>$terminaciones,
            'descrip_coleccion'=>$descrip_coleccion,
            'carrousel_img'=>$carrousel,
            'texto_banner'=>$texto_banner,
            'galeria_productos'=>$galeria_productos,
            'info_adicional'=>$info_adicional,
    
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
     public function grillacoleccionesAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "orden";
        
        $url='grillacolecciones';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from colecciones WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from colecciones WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:colecciones.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
     public function grillacategoriasAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "id";
        
        $url='grillacategorias';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from categorias WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from categorias WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:categorias.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
     public function grillaterminacionesAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "orden";
        
        $url='grillaterminaciones';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from terminaciones WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from terminaciones WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:terminaciones.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
     
    public function grillalevasAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "orden";
        
        $url='grillalevas';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from levas WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from levas WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:levas.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
     
    public function grilladistribuidoresAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "id";
        
        $url='grilladistribuidores';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
                $order_tipo = "asc";
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
        $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from distribuidores WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from distribuidores WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND nombre like '%$texto%'";
            $sql_count.=" AND nombre like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:distribuidores.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function grillaambientacionesAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "orden";
        
        $url='grillaambientaciones';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from ambientaciones WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from ambientaciones WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:ambientaciones.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function grillacombinacionesAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "id";
        
        $url='grillacombinaciones';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from combinaciones WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from combinaciones WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:combinaciones.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }


    public function grillagaleriagralesAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="asc";
        $order = "id";
        
        $url='grillagaleriagrales';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            if (isset($_GET["sort"])){
                $order = $_GET["sort"];
                $order_tipo = "asc";
            }

        }
        
        
         $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from galeriagral WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from galeriagral WHERE 1 = 1";
        
        $sql_count= "SELECT count(1)    
        from galeriagral WHERE 1 = 1";
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
        
        $sql2 = "SELECT *
            from productos WHERE mostrar_galeria = 1 order by orden";
     
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $productos = $conn->fetchAll($this->preventInjection($sql));
        $productos2 = $conn->fetchAll($this->preventInjection($sql2));

        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:galeriagrales.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'productos'=>$productos,
                'productos2'=>$productos2,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }








    public function grillacarrouselAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="desc";
        $order = "id";
        
        $url='grillacarrousel';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            

        }
        
        
        $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from carrousel_imagenes WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from carrousel_imagenes WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $presentaciones = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:grillacarrousel.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'presentaciones'=>$presentaciones,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function grillacarrouselempresaAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $id_usuario  = $_SESSION["usuario"];
        $texto = "";
        $buscador = "none";
        $order_tipo ="desc";
        $order = "id";
        
        $url='grillacarrousel';
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            
           
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
            

        }
        
        
        $actual = 1;
        $limit = 2000;
        
        if (isset($_GET["limit"]))
             $limit = $_GET["limit"];
        
        $paginas = 1; 
        
        if (isset($_GET["page"]))
             $actual = $_GET["page"];
        
        $proxima = $actual + 1;
        
        $ante = $actual - 1;
        
        $link =(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $link = str_replace("&page=$actual", "", $link);
        
        $link = str_replace("?page=$actual", "", $link);
        
        if (strpos($link, "?")>0){
            $anterior = $link."&page=$ante";
            $siguiente = $link."&page=$proxima"; 
        }else{
            $anterior = $link."?page=$ante";
            $siguiente = $link."?page=$proxima";            
        }
        
        $offset = $limit * ($actual-1);
        
        $sql = "SELECT *
                from carrousel_empresa WHERE 1 = 1 ";

                $sql_count= "SELECT count(1)    
                from carrousel_empresa WHERE 1 = 1";
        
        
        if ($texto!=""){
            $sql.=" AND titulo like '%$texto%'";
            $sql_count.=" AND titulo like '%$texto%'";
            }
            
        
        $order_ori = $order;
           
        $sql.=" order by $order $order_tipo LIMIT $limit OFFSET $offset";
        
        $order = $order_ori;
        
        $paginas = $conn->fetchColumn($this->preventInjection($sql_count));

        $presentaciones = $conn->fetchAll($this->preventInjection($sql));
        
        $arr_aux = array();       
        
        $id_usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:grillacarrouselempresa.html.twig', array(
                'user'=>$user,
                'buscador'=>$buscador,
                'presentaciones'=>$presentaciones,
                'order_tipo'=>$order_tipo,
                'order'=>$order,
                'actual'=>$actual,
                'paginas'=>$paginas,
                'anterior'=>$anterior,
                'siguiente'=>$siguiente,
                'limit'=>$limit,
                'texto'=>$texto,
                'url'=>$url,
            
        
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function buscador_productosAction(){
        
        $texto = "";
        $order_tipo ="desc";
        $id_coleccion ="";
        $order = "id";
        $buscador = "";
        $limit = "";
        $url = "";
        
        if ($_GET)
        {
            $buscador = "block";
            if (isset($_GET["order_tipo"]))
                $order_tipo = $_GET["order_tipo"];
            if (isset($_GET["order"])){
                $order = $_GET["order"];
            }
            if (isset($_GET["coleccion"])){
                $id_coleccion = $_GET["coleccion"];
            }
            
            
            if (isset($_GET["texto"]))
                $texto = $_GET["texto"];
               
            if (isset($_GET["limit"]))
                $limit = $_GET["limit"];    
            if (isset($_GET["url"]))
                $url = $_GET["url"];   
          
        }
        
        
        
        return $this->redirect($this->generateUrl($url, array(
            'order_tipo'=>$order_tipo,
            'order'=>$order,           
            'texto'=>$texto,         
            'id_coleccion'=>$id_coleccion,         
            'limit'=>$limit,        
            'url'=>$url,
                )));
    }
    
    public function busquedafAction(){
        
        $arrayB = "";
        $order_tipo ="desc";
        $id_coleccion ="";
        $order = "id";
        $buscador = "";
        $limit = "";
        $url = "";
        if (isset($_POST["buscar"]))
        {
         $arrayB = $_POST["buscar"];
        }
        
        
        
        return $this->redirect($this->generateUrl($url, array(
            'order_tipo'=>$order_tipo,
            'order'=>$order,           
            'texto'=>$texto,         
            'id_coleccion'=>$id_coleccion,         
            'limit'=>$limit,        
            'url'=>$url,
                )));
    }
    

    public function cambiarordenAction()
    {
        //$arr = json_decode($_REQUEST['arr1']);
        //$arr = json_decode($_POST['arr1']);

        $um = $this->get('user_manager');
        
        $user = $um->getLoggedInUser();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();
        $foto_mostrar_galeria = $_POST["foto_mostrar_galeria"];
        $id_foto = $_POST['id_foto'];
        

        $sql = "update productos_imagenes set mostrar_galeria='$foto_mostrar_galeria' where id ='$id_foto'";  
        $conn->exec($this->preventInjection($sql));
    
        
        return $this->redirect($this->generateUrl('grillaproductos'));
    }
    
    
    
    
    public function eliminarfotoAction()
    {
        $id_p = $_GET["id_producto"];
        $id = $_GET["id"];
        
        $um = $this->get('user_manager');
        
       $user = $um->getLoggedInUser();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
       

        $conn = $this->getDoctrine()->getConnection();
        
        
        $sql = "delete from productos_imagenes where id ='$id'";   
        $conn->exec($this->preventInjection($sql));
        
        
        
        return $this->redirect($this->generateUrl('imagenesproductos', array('id'=>$id_p)));
    }
    
    
    public function mostrarImagenAdicionalAction()
    {
        $id = $_GET["id"];
        $visible = $_GET["visible"];
        $um = $this->get('user_manager');
        
       $user = $um->getLoggedInUser();
        
        if(!$user)
            return $this->redirect($this->generateUrl('grillaproductos', array('error'=>'0')));
       

        $conn = $this->getDoctrine()->getConnection();
        
        
        $sql = "update producto_imagenes set mostrar_galeria = '$visible' where id='$id';";   
        $conn->exec($this->preventInjection($sql));
        
        
        
        return $this->redirect($this->generateUrl('imagenesproductos', array('id'=>$id_p)));
    }


    public function consultaAjaxAlfAction()
    {
        // um = $this->get('user_manager');

       // $user = $um->getLoggedInUser();
        
        //$em = $this->getDoctrine()->getEntityManager();
        
       //  if(!$user)
            //return $this->redirect($this->generateUrl('favoritos', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
       
        $nombre = $_POST["nombre"];
        $precio = $_POST["precio"];
    
        //$id = $_POST["id"];
        //eliminamos asignacion de terminaciones
        $sql = "insert into favoritos (idusuario,codProd)
                            values ('$nombre','$precio)"; 
        $conn->exec($this->preventInjection($sql));
        //eliminamos el producto
        //$sql = "delete from productos where id='$id';";
        //$conn->exec($this->preventInjection($sql);
        
        echo($nombre);
        die;
    }
  
    
    
 
    public function buscadorAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
         $conn = $this->getDoctrine()->getConnection();        
        
        $texto = "";
        $diaswa = "";
        $respuesta_wa= "";
        $telefono = "";
        $sql = "";
        if ($_POST)
        {
            $c1= $_POST["c1"];
            $c2= $_POST["c2"];
            $c3= $_POST["c3"];
            $c4= $_POST["c4"];
            $c5= $_POST["c5"];
            $c6= $_POST["c6"];
            $c7= $_POST["c7"];
            $c8= $_POST["c8"];
            $c9= $_POST["c9"];
            $c10= $_POST["c10"];
            $c11= $_POST["c11"];
            
            $v1= $_POST["v1"];
            $v2= $_POST["v2"];
            $v3= $_POST["v3"];
            $v4= $_POST["v4"];
            $v5= $_POST["v5"];
            $v6= $_POST["v6"];
            $v7= $_POST["v7"];
            $v8= $_POST["v8"];
            $v9= $_POST["v9"];
            $v10= $_POST["v10"];
            $v11= $_POST["v11"];
            
            if ($c1 != "" && $v1 != ""){
                $sql.=" AND $c1 like '%$v1%'";
            }
            if ($c2 != "" && $v2 != ""){
                $sql.=" AND $c2 like '%$v2%'";
            }
            if ($c3 != "" && $v3 != ""){
                $sql.=" AND $c3 like '%$v3%'";
            }
            if ($c4 != "" && $v4 != ""){
                $sql.=" AND $c4 like '%$v4%'";
            }
            if ($c5 != "" && $v5 != ""){
                $sql.=" AND $c5 like '%$v5%'";
            }
            if ($c6 != "" && $v6 != ""){
                $sql.=" AND $c6 like '%$v6%'";
            }
            if ($c7 != "" && $v7 != ""){
                $sql.=" AND $c7 like '%$v7%'";
            }
            if ($c8 != "" && $v8 != ""){
                $sql.=" AND $c8 like '%$v8%'";
            }
            if ($c9 != "" && $v9 != ""){
                $sql.=" AND $c9 like '%$v9%'";
            }
            if ($c10 != "" && $v10 != ""){
                $sql.=" AND $c10 like '%$v10%'";
            }
            if ($c11 != "" && $v11 != ""){
                $sql.=" AND $c11 like '%$v11%'";
            }
        }
        
        $sql_propieadades = "select * from propiedades_view
        where 1 = 1 $sql;";
        
        $propiedades = $conn->fetchAll($this->preventInjection($sql_propieadades));
        
         $sql_campos= "select column_name as valor from information_schema.columns
        where table_name = 'propiedades_view'
        order by column_name;";
        
        $campos = $conn->fetchAll($this->preventInjection($sql_campos));
              
        $view = $this->renderView('AmapolannUserBundle:Account:buscador.html.twig', array(
                'user'=>$user,
                'campos'=>$campos,
                'propiedades'=>$propiedades
                
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
   
    public function portadacoleccionAction() //modificado por los directorios
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_coleccion = $_POST["id_coleccion"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadacolecciones/imagenes/$id_coleccion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadacolecciones/imagenes/$id_coleccion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update colecciones set imagen_portada = '$target_file' where id='$id_coleccion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    public function portadamenuAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_coleccion = $_POST["id_coleccion"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadamenu/imagenes/$id_coleccion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadamenu/imagenes/$id_coleccion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update colecciones set imagen_menu = '$target_file' where id='$id_coleccion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    public function portadapresentacionAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_presentacion = $_POST["id_presentacion"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadapresentaciones/imagenes/$id_presentacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadapresentaciones/imagenes/$id_presentacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update carrousel_imagenes set imagen = '$target_file' where id='$id_presentacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    public function portadapresentacionempresaAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_presentacion = $_POST["id_presentacion"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadapresentacionesempresa/imagenes/$id_presentacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadapresentacionesempresa/imagenes/$id_presentacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update carrousel_empresa set imagen = '$target_file' where id='$id_presentacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    public function portadaproductoAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_producto = $_POST["id_producto"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadaproductos/imagenes/$id_producto/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadaproductos/imagenes/$id_producto/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update productos set imagen_portada = '$target_file' where id='$id_producto';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    
    public function planoproductoAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_producto = $_POST["id_producto"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_plano"]["name"])&&($_FILES["imagen_plano"]["name"])!=null&&($_FILES["imagen_plano"]["name"]!=""))){
                  
                $target_file = "../planosproductos/imagenes/$id_producto/" . basename($_FILES["imagen_plano"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../planosproductos/imagenes/$id_producto/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_plano"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update productos set imagen_plano = '$target_file' where id='$id_producto';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    public function pdfproductoAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_producto = $_POST["id_producto"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["pdf_plano"]["name"])&&($_FILES["pdf_plano"]["name"])!=null&&($_FILES["pdf_plano"]["name"]!=""))){
                  
                $target_file = "../planosproductos/planospdf/$id_producto/" . basename($_FILES["pdf_plano"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../planosproductos/planospdf/$id_producto/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "pdf";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "pdf" ||$imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("pdf", "pdf", $target_file);
                    $upload = move_uploaded_file($_FILES["pdf_plano"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update productos set pdf_plano = '$target_file' where id='$id_producto';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    
    public function portadaterminacionAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_terminacion = $_POST["id_terminacion"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadaterminaciones/imagenes/$id_terminacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadaterminaciones/imagenes/$id_terminacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update terminaciones set imagen_portada = '$target_file' where id='$id_terminacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    
    public function portadalevaAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_leva = $_POST["id_leva"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadalevas/imagenes/$id_leva/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadalevas/imagenes/$id_leva/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update levas set imagen_portada = '$target_file' where id='$id_leva';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
    public function portadadistribuidoresAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_distribuidor = $_POST["id"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadadistribuidores/imagenes/$id_distribuidor/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadadistribuidores/imagenes/$id_distribuidor/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update distribuidores set imagen = '$target_file' where id='$id_distribuidor';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    public function portadaambientacionAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_ambientacion = $_POST["id"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadaambientaciones/imagenes/$id_ambientacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadaambientaciones/imagenes/$id_ambientacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update ambientaciones set imagen = '$target_file' where id='$id_ambientacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    public function favoritosAction()
    {  
        $conn = $this->getDoctrine()->getConnection();
        
        if(isset($_GET["id_prod"])){
            $id_producto=$_GET["id_prod"];
            $sql="SELECT * FROM terminaciones WHERE id in (select id_terminacion from terminaciones_por_producto where id_producto='$id_producto')";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $titulo_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT descripcion FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $descripcion_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT imagen_portada FROM colecciones WHERE id IN(SELECT id_categoria FROM productos WHERE id ='$id_producto')";
            $portada_categoria=$conn->fetchColumn($this->preventInjection($sql));
            
            $sql="SELECT titulo FROM productos WHERE id = '$id_producto'";
            $titulo_producto=$conn->fetchColumn($this->preventInjection($sql));
        }
        else{
            $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
            $terminaciones_producto=$conn->fetchAll($this->preventInjection($sql));
            
            $sql="SELECT * FROM levas WHERE 1=1 order by codigo asc";
            $levas=$conn->fetchAll($this->preventInjection($sql));
            
            $titulo_categoria="";
            $titulo_producto="";
            $descripcion_categoria="";
            $portada_categoria="";
        }
        $sql="SELECT * FROM colecciones WHERE 1=1 and activo=1 order by orden asc";
        $colecciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT * FROM terminaciones WHERE 1=1 order by codigo asc";
        $terminaciones=$conn->fetchAll($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Terminaciones'";
        $descrip_terminacion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT texto FROM textos_predefinidos WHERE titulo='Descripcion generica de Colecciones'";
        $descrip_coleccion=$conn->fetchColumn($this->preventInjection($sql));
        
        $sql="SELECT * FROM distribuidores";
            $distribuidores=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM favoritos";
        $favoritos=$conn->fetchAll($this->preventInjection($sql));

        $sql="SELECT * FROM productos";
        $productos=$conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:favoritosfront.html.twig', array(
            'colecciones'=>$colecciones,    
            'terminaciones'=>$terminaciones,
            'terminaciones_producto'=>$terminaciones_producto,
            'titulo_categoria'=>$titulo_categoria,
            'descripcion_categoria'=>$descripcion_categoria,
            'titulo_producto'=>$titulo_producto,
            'portada_categoria'=>$portada_categoria,
            'productos'=>$productos,
            'descrip_terminacion'=>$descrip_terminacion,
            'descrip_coleccion'=>$descrip_coleccion,
            'levas'=>$levas,
            'favoritos'=>$favoritos,
            'distribuidores'=>$distribuidores,
            
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 

        /*
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_ambientacion = $_POST["id"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "portadaambientaciones/imagenes/$id_ambientacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "portadaambientaciones/imagenes/$id_ambientacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update ambientaciones set imagen = '$target_file' where id='$id_ambientacion';";
                            $conn->exec($this->preventInjection($sql);
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
        */
    }

    public function portadacombinacionAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_combinacion = $_POST["id"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadacombinaciones/imagenes/$id_combinacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadacombinaciones/imagenes/$id_combinacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update combinaciones set imagen = '$target_file' where id='$id_combinacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    public function portadagaleriagralAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $id_galeriagral = $_POST["id"];
        //$img_portada = $_FILES["imagen_por"];
        
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadagaleriagrales/imagenes/$id_galeriagral/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadagaleriagrales/imagenes/$id_galeriagral/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "update galeriagral set imagen = '$target_file' where id='$id_galeriagral';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    
    public function imagenesproductosAction()
    {  
        $user = "notused";
        $conn = $this->getDoctrine()->getConnection();
        
        $idproducto = $_GET["id"];
        
        if ($_FILES && (isset($_FILES["files"]["name"][0])&&($_FILES["files"]["name"][0])!=null&&($_FILES["files"]["name"][0]!=""))){
            
            for ($i=0;$i<count($_FILES["files"]["size"]);$i++){
                
               
            if (isset($_FILES["files"]["name"][$i])&&($_FILES["files"]["name"][$i])!=null&&($_FILES["files"]["name"][$i]!="")){
             
                $target_file = "../productos/imagenes/$idproducto/" . basename($_FILES["files"]["name"][$i]);
                $rand = rand(1,999999);
                
                $directory = "../productos/imagenes/$idproducto/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                    
                }
                

                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg"
                        || $imageFileType == "gif" )
                {
                
                    
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                        $target_file = str_replace("jpeg", "jpg", $target_file);
                        $upload = move_uploaded_file($_FILES["files"]["tmp_name"][$i], $target_file);
                       
                        if ($upload){
                           $sql = "insert into productos_imagenes (id_producto,imagen, orden)
                            values ('$idproducto','$target_file','1000')";
                           
                            $conn->exec($this->preventInjection($sql));
           
                           
                        }
                    
                    
                }
            
               
            }
          }
          echo ("{status:ok}");die;
        }
        
        if ($idproducto!="0")
            $arrayFotos = $conn->fetchAll($this->preventInjection("select * from productos_imagenes where id_producto='$idproducto' order by orden"));
        else
            $arrayFotos = array();
         
       
         
        $view = $this->renderView('AmapolannUserBundle:Account:imagenesproductos.html.twig', array(
                'user'=>$user,
                'fotos'=>$arrayFotos,
                'id'=>$idproducto
        ));

        $response = new Response($view);


        return $response;            
             
       
    }
    
    public function guardarnueterminacionAction(){
        
        $um = $this->get('user_manager');
        $user = $um->getLoggedInUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_producto = $_POST["id"];
        $id_terminacion = $_POST["id_ter"];
        
        
        if($id_terminacion==null || $id_terminacion==""){
            echo("La terminación no existe");
            die;
        }
        
        $arr_ids = explode(",", $id_terminacion);
        
        foreach ($arr_ids as $id_ter) {
            
        
        
            $sql="select id_producto from terminaciones_por_producto where id_producto = '$id_producto' and id_terminacion = '$id_ter';";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            $bandera_existe = true;
            if ($existe == null || $existe != "$id_producto"){
                        $bandera_existe = false;
                        $sql_insert= "INSERT INTO terminaciones_por_producto (id_producto,id_terminacion) values($id_producto,$id_ter);";
                        $conn->exec($this->preventInjection($sql_insert));
                       
                    }
            else{

            } 
        }
        
         echo("ok");
                        die;
    }
    
        
    public function guardar_terminacion_coleccionAction(){
        
        $um = $this->get('user_manager');
        $user = $um->getLoggedInUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_coleccion = $_POST["id"];
        $id_terminacion = $_POST["id_ter"];
        
        
        if($id_terminacion==null || $id_terminacion==""){
            echo("La terminación no existe");
            die;
        }
        
        $sql="select id_coleccion from terminaciones_por_coleccion where id_coleccion = '$id_coleccion' and id_terminacion = '$id_terminacion';";
        $existe = $conn->fetchColumn($this->preventInjection($sql));
        $bandera_existe = true;
        if ($existe == null || $existe != "$id_coleccion"){
                    $bandera_existe = false;
                    $sql_insert= "INSERT INTO terminaciones_por_coleccion (id_coleccion,id_terminacion) values($id_coleccion,$id_terminacion);";
                    $conn->exec($this->preventInjection($sql_insert));
                    
                    //ME TRAIGO TODOS LOS PRODUCTOS DE LA COLECCION
                    $sql="SELECT id from productos where id_categoria = '$id_coleccion';";
                    $productos=$conn->fetchAll($this->preventInjection($sql));
                    foreach ($productos as $key) {
                        $id=$key["id"];
                        
                        $sql="select id_producto from terminaciones_por_producto where id_producto = '$id' and id_terminacion = '$id_terminacion';";
                        $existe = $conn->fetchColumn($this->preventInjection($sql));
                        
                        $bandera_existe = true;
                        if ($existe == null || $existe != "$id"){
                                    $bandera_existe = false;
                                    $sql_insert= "INSERT INTO terminaciones_por_producto (id_producto,id_terminacion) values('$id','$id_terminacion');";
                                    $conn->exec($this->preventInjection($sql_insert));
                        }
                    }
                    
                    echo("ok");
                    die;
                }
        else{
            echo("La terminación ya esta asignada para este producto.");
            die;
        }        
    }
    
    public function guardar_leva_coleccionAction(){
        
        $um = $this->get('user_manager');
        $user = $um->getLoggedInUser();
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_coleccion = $_POST["id"];
        $id_leva = $_POST["id_leva"];
        
        
        if($id_leva==null || $id_leva==""){
            echo("La leva no existe");
            die;
        }
        
        $sql="select id_coleccion from levas_por_coleccion where id_coleccion = '$id_coleccion' and id_leva = '$id_leva';";
        $existe = $conn->fetchColumn($this->preventInjection($sql));
        $bandera_existe = true;
        if ($existe == null || $existe != "$id_coleccion"){
                    $bandera_existe = false;
                    $sql_insert= "INSERT INTO levas_por_coleccion (id_coleccion,id_leva) values($id_coleccion,$id_leva);";
                    $conn->exec($this->preventInjection($sql_insert));
                    
                    echo("ok");
                    die;
                }
        else{
            echo("La leva ya esta asignada para esta colección.");
            die;
        }        
    }
    
    
    public function productoabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
        
        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from productos where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillaproductos'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            $id2 = $_POST["id"];
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $codigo = (isset($_POST["codigo"])) ? $_POST["codigo"] : "";
            $detalles = (isset($_POST["detalles"])) ? $_POST["detalles"] : "";
            if (isset($_POST["detalles"]))
                $detalles = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $detalles);
            $alto = (isset($_POST["alto"])) ? $_POST["alto"] : "";
            $ancho = (isset($_POST["ancho"])) ? $_POST["ancho"] : "";
            $profundidad = (isset($_POST["profundidad"])) ? $_POST["profundidad"] : "";
            $coleccion = (isset($_POST["coleccion"])) ? $_POST["coleccion"] : "";
            $activo = (isset($_POST["activo"])) ? $_POST["activo"] : "";
            $categoria = (isset($_POST["categoria"])) ? $_POST["categoria"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            $mostrar = (isset($_POST["mostrar"])) ? $_POST["mostrar"] : "";
            $tipoPlano = (isset($_POST["tipoPlano"])) ? $_POST["tipoPlano"] : "";
            $mostrarPlano = (isset($_POST["mostrarPlano"])) ? $_POST["mostrarPlano"] : "";
            $keywords = (isset($_POST["keywords"])) ? $_POST["keywords"] : "";
            if (isset($_POST["keywords"]))
                $keywords = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $keywords);
            //$mostrarImagenGaleria = (isset($_POST["mostrarImagenGaleria"])) ? $_POST["mostrarImagenGaleria"] : "";
            if ($id == "0")
            {               
                $sql = "INSERT INTO `productos`
                        (`titulo`,
                        `id_categoria`,
                        `codigo`,
                        `id_cat`,
                        `descripcion`,
                        `alto`,
                        `ancho`,
                        `profundidad`,
                        `activo`,
                        `orden`,
                        `mostrar_galeria`,
                        `tipoPlano`,
                        `mostrar_plano`,
                        `keywords`)
                        VALUES
                        ('$titulo',
                        '$coleccion',
                        '$codigo',
                        '$categoria',
                        '$detalles',
                        '$alto',
                        '$ancho',
                        '$profundidad',
                        '$activo',
                        '$orden',
                        '$mostrar',
                        '$tipoPlano',
                        '$mostrarPlano',
                        '$keywords');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();     
                
                //Asignamos terminaciones de coleccion al producto creado.
                
                $sql="SELECT id_terminacion FROM terminaciones_por_coleccion WHERE id_coleccion='$coleccion'";
                $termis_de_coleccion=$conn->fetchAll($this->preventInjection($sql));
                
                foreach ($termis_de_coleccion as $key) {
                        $id_terminacion = $key["id_terminacion"];
                        
                        $sql_insert= "INSERT INTO terminaciones_por_producto (id_producto,id_terminacion) values('$id','$id_terminacion');";
                        $conn->exec($this->preventInjection($sql_insert));
                        
                }
                
                return $this->redirect($this->generateUrl('productoabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE productos SET
                        titulo =   '$titulo',
                        id_categoria= '$coleccion',
                        codigo= '$codigo',
                        id_cat= '$categoria',
                        descripcion= '$detalles',
                        alto= '$alto',
                        ancho= '$ancho',
                        profundidad= '$profundidad',
                        tipoPlano= '$tipoPlano',
                        activo='$activo', 
                        orden='$orden', 
                        mostrar_galeria='$mostrar',
                        mostrar_plano='$mostrarPlano',
                        keywords='$keywords' 
                        WHERE `ID` = $id;    
                        ";

            $conn->exec($this->preventInjection($sql));
  
       

                return $this->redirect($this->generateUrl('grillaproductos'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagenes = array();
        $terminaciones_x_prod = null;
        $sql="select * from colecciones where 1 = 1";
        $colecciones = $conn->fetchAll($this->preventInjection($sql));
        $sql="select * from terminaciones where 1 = 1";
        $terminaciones = $conn->fetchAll($this->preventInjection($sql));
        $sql="select * from categorias where 1 = 1";
        $categorias = $conn->fetchAll($this->preventInjection($sql));
        $sql="select * from productos_imagenes where 1 = 1";
        $imagenes3 = $conn->fetchAll($this->preventInjection($sql));

        
        
        if ($id>0){
           
            $sql= "select * from productos_view where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));


            $sql= "select * from productos_imagenes where id_producto = '$id' order by orden;";

            $imagenes= $conn->fetchAll($this->preventInjection($sql));
            
            $sqlp= "select * from terminaciones_por_producto_view where id_producto = '$id';";
            $terminaciones_x_prod=$conn->fetchAll($this->preventInjection($sqlp));
            
           
            $id_usuario = $_SESSION["usuario"];
           
        }       
       
 
        $view = $this->renderView('AmapolannUserBundle:Account:producto.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'propiedad'=>$propiedad,
                'imagenes'=>$imagenes,
                'terminaciones'=>$terminaciones,
                'terminaciones_x_prod'=>$terminaciones_x_prod,
                'colecciones'=>$colecciones,
                'categorias'=>$categorias,
                'imagenes3'=>$imagenes3,
                
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function terminacionabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from terminaciones where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillaterminaciones'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $codigo = (isset($_POST["codigo"])) ? $_POST["codigo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            $detalles = (isset($_POST["detalles"])) ? $_POST["detalles"] : "";
            if (isset($_POST["detalles"]))
                $detalles = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $detalles);
           
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `terminaciones`
                        (`titulo`,
                        `codigo`,
                        `orden`,
                        `descripcion`)
                        VALUES
                        ('$titulo',
                         '$codigo',   
                         '$orden',   
                        '$detalles');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('terminacionabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE terminaciones SET
                        titulo =   '$titulo',
                        codigo =   '$codigo',
                        orden =   '$orden',
                        descripcion= '$detalles'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillaterminaciones'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from terminaciones where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen_portada from terminaciones where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:terminacion.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }


    public function galeriagralabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from terminaciones where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillagaleriagrales'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            $imagen = (isset($_POST["imagen"])) ? $_POST["imagen"] : "";
            
           
            if ($id == "0")
            {               
                $sql = "INSERT INTO `galeriagral`
                        (`titulo`,
                        `orden`)
                        VALUES
                        ('$titulo',
                         '$orden');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('galeriagralabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE galeriagral SET
                        titulo =   '$titulo',
                        orden =   '$orden'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillagaleriagrales'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from galeriagral where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen from galeriagral where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:galeriagral.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function levaabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from levas where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillalevas'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $codigo = (isset($_POST["codigo"])) ? $_POST["codigo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            $detalles = (isset($_POST["detalles"])) ? $_POST["detalles"] : "";
            if (isset($_POST["detalles"]))
                $detalles = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $detalles);
           
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `levas`
                        (`titulo`,
                        `codigo`,
                        `orden`,
                        `descripcion`)
                        VALUES
                        ('$titulo',
                         '$codigo',   
                         '$orden',   
                        '$detalles');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('levaabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE levas SET
                        titulo =   '$titulo',
                        codigo =   '$codigo',
                        orden =   '$orden',
                        descripcion= '$detalles'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillalevas'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from levas where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen_portada from levas where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:leva.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    

    public function distribuidorabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from distribuidores where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grilladistribuidores'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            $nombre = (isset($_POST["nombre"])) ? $_POST["nombre"] : "";
            $direccion = (isset($_POST["direccion"])) ? $_POST["direccion"] : "";
            $localidad = (isset($_POST["localidad"])) ? $_POST["localidad"] : "";
            $provincia = (isset($_POST["provincia"])) ? $_POST["provincia"] : "";
            $tel1 = (isset($_POST["tel1"])) ? $_POST["tel1"] : "";
            $tel2 = (isset($_POST["tel2"])) ? $_POST["tel2"] : "";
            $imagen = (isset($_POST["imagen"])) ? $_POST["imagen"] : "";
            $coordenadas = (isset($_POST["coordenadas"])) ? $_POST["coordenadas"] : "";
            $urlubicacion = (isset($_POST["urlubicacion"])) ? $_POST["urlubicacion"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            
            
           
            if ($id == "0")
            {               
                $sql = "INSERT INTO `distribuidores` (`nombre`, `direccion`, `localidad`, `provincia`, `tel1`, `tel2`, `coordenadas`, `urlubicacion`, `orden`) VALUES ('$nombre', '$direccion', '$localidad', '$provincia', '$tel1', '$tel2', '$coordenadas', '$urlubicacion', '$orden');";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('distribuidorabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE distribuidores SET
                        nombre =   '$nombre',
                        direccion =   '$direccion',
                        localidad =   '$localidad',
                        provincia =   '$provincia',
                        tel1 =   '$tel1',
                        tel2 =   '$tel2',
                        coordenadas =   '$coordenadas',
                        urlubicacion =   '$urlubicacion',
                        orden =   '$orden'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grilladistribuidores'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from distribuidores where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen from distribuidores where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:distribuidor.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function ambientacionabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from ambientaciones where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillaambientaciones'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $griferia = (isset($_POST["griferia"])) ? $_POST["griferia"] : "";
            $bacha = (isset($_POST["bacha"])) ? $_POST["bacha"] : "";
            $revestimiento1 = (isset($_POST["revestimiento1"])) ? $_POST["revestimiento1"] : "";
            $revestimiento2 = (isset($_POST["revestimiento2"])) ? $_POST["revestimiento2"] : "";
            $mesada = (isset($_POST["mesada"])) ? $_POST["mesada"] : "";
            $imagen = (isset($_POST["imagen"])) ? $_POST["imagen"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            $revestimiento3 = (isset($_POST["revestimiento3"])) ? $_POST["revestimiento3"] : "";
            $accesorios = (isset($_POST["accesorios"])) ? $_POST["accesorios"] : "";
            $acabados = (isset($_POST["acabados"])) ? $_POST["acabados"] : "";

            if ($id == "0")
            {               
                $sql = "INSERT INTO `ambientaciones`
                        (`titulo`,
                        `griferia`,
                        `bacha`,
                        `revestimiento1`,
                        `revestimiento2`,
                        `mesada`,
                        `orden`,
                        `revestimiento3`,
                        `accesorios`,
                        `acabados`)
                        VALUES
                        ('$titulo',
                         '$griferia',
                         '$bacha',    
                         '$revestimiento1', 
                         '$revestimiento2', 
                         '$mesada', 
                         '$orden',  
                         '$revestimiento3',  
                         '$accesorios',  
                         '$acabados');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('ambientacionabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE ambientaciones SET
                        titulo =   '$titulo',
                        griferia =   '$griferia',
                        bacha =   '$bacha',
                        revestimiento1 =   '$revestimiento1',
                        revestimiento2 =   '$revestimiento2',
                        mesada =   '$mesada',
                        orden =   '$orden',
                        revestimiento3 =   '$revestimiento3',
                        accesorios =   '$accesorios',
                        acabados= '$acabados'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillaambientaciones'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from ambientaciones where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen from ambientaciones where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:ambientacion.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }


    public function combinacionabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from combinaciones where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacombinaciones'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $explicacion = (isset($_POST["explicacion"])) ? $_POST["explicacion"] : "";
            $autor = (isset($_POST["autor"])) ? $_POST["autor"] : "";
            $imagen = (isset($_POST["imagen"])) ? $_POST["imagen"] : "";
            
           
            if ($id == "0")
            {               
                $sql = "INSERT INTO `combinaciones`
                        (`titulo`,
                        `explicacion`,
                        `autor`)
                        VALUES
                        ('$titulo',
                         '$explicacion',
                         '$autor');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('combinacionabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE combinaciones SET
                        titulo =   '$titulo',
                        explicacion =   '$explicacion',
                        autor =   '$autor'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacombinaciones'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
       
        if ($id>0){
           
            $sql= "select * from combinaciones where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql= "select imagen from combinaciones where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:combinacion.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'imagen'=>$imagen,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }


    public function carrouselabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
       
        if (isset($_GET["id"])){
            $id = $_GET["id"];  
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from carrousel_imagenes where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacarrousel'));
            }
           
        }        
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $subtitulo = (isset($_POST["subtitulo"])) ? $_POST["subtitulo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            
            
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `carrousel_imagenes`
                        (`titulo`,
                        `subtitulo`,
                        `orden`)
                        VALUES
                        ('$titulo',
                        '$subtitulo',
                        $orden);
                        ";                
                
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('carrouselabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = "UPDATE carrousel_imagenes SET titulo = '$titulo' ,subtitulo= '$subtitulo' , orden = $orden WHERE id = '$id';";
                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacarrousel'));
            }
            
            
        }
        
          
        $propiedad = null;
        $productos_coleccion = array();
       
        if ($id>0){
           
            $sql= "select * from carrousel_imagenes where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:imgcarrousel.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function carrouselempresaabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
       
        if (isset($_GET["id"])){
            $id = $_GET["id"];  
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from carrousel_empresa where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacarrouselempresa'));
            }
           
        }        
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $subtitulo = (isset($_POST["subtitulo"])) ? $_POST["subtitulo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            
            
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `carrousel_empresa`
                        (`titulo`,
                        `subtitulo`,
                        `orden`)
                        VALUES
                        ('$titulo',
                        '$subtitulo',
                        $orden);
                        ";                
                
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('carrouselempresaabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = "UPDATE carrousel_empresa SET titulo = '$titulo' ,subtitulo= '$subtitulo' , orden = $orden WHERE id = '$id';";
                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacarrouselempresa'));
            }
            
            
        }
        
          
        $propiedad = null;
        $productos_coleccion = array();
       
        if ($id>0){
           
            $sql= "select * from carrousel_empresa where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:imgcarrouselempresa.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function coleccionabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
       
        if (isset($_GET["id"])){
            $id = $_GET["id"];  
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from colecciones where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacolecciones'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $codigo = (isset($_POST["codigo"])) ? $_POST["codigo"] : "";
            $detalles = (isset($_POST["detalles"])) ? $_POST["detalles"] : "";
            $activo = (isset($_POST["activo"])) ? $_POST["activo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            if (isset($_POST["detalles"]))
                $detalles = str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br>", $detalles);
           
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `colecciones`
                        (`titulo`,
                        `codigo`,
                        `descripcion`,
                        `activo`,
                        `orden`)
                        VALUES
                        ('$titulo',
                        '$codigo',
                        '$detalles',
                        '$activo',
                        '$orden');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('coleccionabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE colecciones SET
                        titulo =   '$titulo',
                        codigo= '$codigo',
                        descripcion= '$detalles',
                        activo='$activo',
                        orden='$orden'
                        WHERE `ID` = $id;";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacolecciones'));
            }
            
            
        }
        
          
        $propiedad = null;
        $imagen = array();
        $productos_coleccion = array();
        $terminaciones_x_coleccion=null;
        $terminaciones=null;
        $levas=null;
        $levas_x_coleccion=null;
        if ($id>0){
           
            
            $sql= "select * from colecciones where id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql="select * from productos where id_categoria='$id'";
            $productos_coleccion=$conn->fetchAll($this->preventInjection($sql));
            
            $sql= "select imagen_portada from colecciones where id = '$id';";

            $imagen= $conn->fetchColumn($this->preventInjection($sql));
           
            $sql="SELECT * FROM terminaciones_por_coleccion_view WHERE id_coleccion='$id'";
            $terminaciones_x_coleccion=$conn->fetchAll($this->preventInjection($sql));
           
            $sql="SELECT * FROM levas_por_coleccion_view WHERE id_coleccion='$id'";
            $levas_x_coleccion=$conn->fetchAll($this->preventInjection($sql));
           
            $sql="SELECT * FROM levas WHERE 1=1";
            $levas=$conn->fetchAll($this->preventInjection($sql));
           
            $sql="SELECT * FROM terminaciones WHERE 1=1";
            $terminaciones=$conn->fetchAll($this->preventInjection($sql));
            
        }    
 
        $view = $this->renderView('AmapolannUserBundle:Account:coleccion.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'propiedad'=>$propiedad,
                'imagen'=>$imagen,
                'productos_coleccion'=>$productos_coleccion,
                'terminaciones_x_coleccion'=>$terminaciones_x_coleccion,
                'terminaciones'=>$terminaciones,
                'levas'=>$levas,
                'levas_x_coleccion'=>$levas_x_coleccion,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    
    public function categoriaabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
       
        if (isset($_GET["id"])){
            $id = $_GET["id"];  
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from categorias where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacategorias'));
            }
            
            $id_usuario = $_SESSION["usuario"];
           
        }
        else{
            $id_usuario = $_SESSION["usuario"];           
            
        }
                
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $codigo = (isset($_POST["codigo"])) ? $_POST["codigo"] : "";
           
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `categorias`
                        (`titulo`,`codigo`)
                        VALUES
                        ('$titulo',
                         '$codigo');
                        ";                
            
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('categoriaabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = " UPDATE categorias SET
                        titulo =   '$titulo', codigo='$codigo'"
                         . "WHERE `ID` = $id;
                            ";

                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacategorias'));
            }
            
            
        }
        
          
        $categoria = null;
        $productos_categoria = array();
       
        if ($id>0){
           
            $sql= "select * from categorias where id = '$id';";

            $categoria = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
            $sql="select * from productos where id_cat='$id'";
            $productos_categoria=$conn->fetchAll($this->preventInjection($sql));
            
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:categoria.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'categoria'=>$categoria,
                'productos_categoria'=>$productos_categoria,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function eliminarusAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        $sql = "UPDATE usuario set habilitado = 0 where id='$id';";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }
    
    public function eliminarprodAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        //eliminamos asignacion de terminaciones
        $sql = "delete from terminaciones_por_producto where id_producto='$id';";
        $conn->exec($this->preventInjection($sql));
        //eliminamos el producto
        $sql = "delete from productos where id='$id';";
        $conn->exec($this->preventInjection($sql));
        
        echo($id);
        die;
    }
    
    public function eliminarterminacionAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino todas las asignaciones de la terminacion a productos
        $sql = "delete from terminaciones_por_producto where id_terminacion='$id';";
        $conn->exec($this->preventInjection($sql));
        //elimino la terminacion.
        $sql = "delete from terminaciones where id='$id';";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }
    
    public function eliminarlevaAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino todas las asignaciones de la leva a colecciones
        $sql = "delete from levas_por_coleccion where id_leva='$id';";
        $conn->exec($this->preventInjection($sql));
        //elimino la leva.
        $sql = "delete from levas where id='$id';";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }
    
    public function eliminarambientacionAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino ambientacion
        $sql = "delete from ambientaciones where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
    }

    public function actualizarOrdenGaleriaAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        $orden = $_POST["orden"];
        
        //elimino ambientacion
        $sql = "update from SET orden = $orden where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
    }

    public function eliminarcombinacionAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino combinacion
        $sql = "delete from combinaciones where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
    }

    public function eliminardistribuidorAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino combinacion
        $sql = "delete from distribuidores where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
    }

    public function eliminarcoleccionAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        //Me fijo si tiene productos asociados
        $elem=$conn->fetchColumn($this->preventInjection("select id from productos where id_categoria = '$id' limit 1"));
        if($elem != "" && $elem != null){
            echo("not");die;
        }
        //elimino la coleccion.
        else{
            $sql = "delete from colecciones where id='$id';";
            $conn->exec($this->preventInjection($sql));

            echo("ok");
            die;
        }
        
    }
    public function eliminarcategoriaAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        //Me fijo si tiene productos asociados
        $elem=$conn->fetchColumn($this->preventInjection("select id from productos where id_cat = '$id' limit 1"));
        if($elem != "" && $elem != null){
            echo("not");die;
        }
        //elimino la coleccion.
        else{
            $sql = "delete from categorias where id='$id';";
            $conn->exec($this->preventInjection($sql));

            echo("ok");
            die;
        }
        
    }
    
    public function eliminarpresentacionAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        $sql = "delete from carrousel_imagenes where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
        
        
    }

    public function eliminarpresentacionempresaAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        $sql = "delete from carrousel_empresa where id='$id';";
        $conn->exec($this->preventInjection($sql));

        echo("ok");
        die;
        
        
    }
    
    public function eliminar_ter_prodAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino la asignacion de la terminacion al producto.
        $sql = "delete from terminaciones_por_producto where id='$id';";
        $conn->exec($this->preventInjection($sql));
        
        
        echo("ok");
        die;
    }
    
    
    public function eliminar_ter_colAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_coleccion = $_POST["id_coleccion"];
        $id_terminacion = $_POST["id_terminacion"];
        
        //elimino la asignacion de la terminacion a la coleccion.
        $sql = "delete from terminaciones_por_coleccion where id_coleccion='$id_coleccion' and id_terminacion='$id_terminacion';";
        $conn->exec($this->preventInjection($sql));
        
        //recorrer todos los productos, y si tienen la terminacion asignada, borrarla.
        
        $sql="SELECT id from productos where id_categoria = '$id_coleccion';";
        $productos=$conn->fetchAll($this->preventInjection($sql));
        
        foreach ($productos as $key) {
            $id=$key["id"];

            $sql="select id_producto from terminaciones_por_producto where id_producto = '$id' and id_terminacion = '$id_terminacion';";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            $bandera_existe = true;
            if ($existe != null || $existe == "$id"){
                        $bandera_existe = false;
                        $sql_delete= "DELETE FROM terminaciones_por_producto WHERE id_producto='$id' AND id_terminacion='$id_terminacion';";
                        $conn->exec($this->preventInjection($sql_delete));
            }
        }
        
        
        echo("ok");
        die;
    }
    
    
    public function eliminar_leva_colAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_coleccion = $_POST["id_coleccion"];
        $id_leva = $_POST["id_leva"];
        
        //elimino la asignacion de la terminacion a la coleccion.
        $sql = "delete from levas_por_coleccion where id_coleccion='$id_coleccion' and id_leva='$id_leva';";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }

    public function eliminar_ambientacion_colAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino la asignacion de la terminacion a la coleccion.
        $sql = "delete from ambientacion where id='$id'";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }

    public function eliminar_combinacion_colAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id = $_POST["id"];
        
        //elimino la asignacion de la terminacion a la coleccion.
        $sql = "delete from combinaciones where id='$id'";
        $conn->exec($this->preventInjection($sql));
        
        echo("ok");
        die;
    }
    

    public function logoutAction()
    {
       $um = $this->get('user_manager');
       $um->logout(); 
       return $this->loginabmAction();
    }

    
    public function loginabmAction($error="")
    {
        
        if (isset($_GET["error"]))
            $error = $_GET["error"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:login.html.twig', array(
            'error'=>$error
            ));

        return new Response($view);
                
               
    }  
    
    public function loginabmAdminAction($error="")
    {
        
        if (isset($_GET["error"]))
            $error = $_GET["error"];
        $view = $this->renderView('AmapolannUserBundle:Account:login.html.twig', array(
            'error'=>$error
            ));

        

        return new Response($view);
                
               
    }  
    
    
     public function HeaderAction($user=null)
    {  
        $usuario = "";
        if (isset($_SESSION["usuario"]))
            $usuario = $_SESSION["usuario"];
        
        $view = $this->renderView('AmapolannUserBundle:Account:headerf.html.twig', array(
            'user'=>$user,
            'usuario'=>$usuario,
          
        ));


        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response;            
             
       
    }
    
    
    public function progresoAction($nombreSeccion="",$seccion="")
    {  
       
         $conn = $this->getDoctrine()->getConnection();     
        
         $id_usuario = $_SESSION["usuario"];       
        
       
        
        $view = $this->renderView('AmapolannUserBundle:Account:progresof.html.twig', array(
          
        ));


        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response;            
             
       
    }
    
    
   
    public function nuevousuarioAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        
        $id_usuario = $_SESSION["usuario"];
        
        
        $id = 0;
                
        
        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }
        
        if ($_POST){
            $id = $_POST["id"];
            
            $nombre = (isset($_POST["nombre"])) ? $_POST["nombre"] : "";
            $email = (isset($_POST["email"])) ? $_POST["email"] : "";
            $contrasena = (isset($_POST["contrasena"])) ? $_POST["contrasena"] : "";
            $habilitado = (isset($_POST["habilitado"])) ? $_POST["habilitado"] : "";
           
            
            if ($id=="0")
            {
                
                $sql = "INSERT INTO `usuario`
                        (`nombre`,
                        `email`,
                        `habilitado`,
                        `contrasena`)
                         VALUES
                         ('$nombre',
                         '$email',
                         $habilitado,
                         '$contrasena'   
                         );
                         ";
                
           
                
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();
                
                return $this->redirect($this->generateUrl('usuarios'));
                
            }
            else{
                 $sql = " UPDATE usuario SET
                        nombre =   '$nombre',
                        email= '$email',
                        contrasena= '$contrasena',
                        habilitado= $habilitado 
                       "
                         . "WHERE `ID` = $id;
                            ";
                                
                 $conn->exec($this->preventInjection($sql));
                
                
                return $this->redirect($this->generateUrl('usuarios'));
            }
        }
        $puntos=array();
        $usuario = null;
        if ($id>0){
            $sql= "select * from usuario where id = '$id';";

            $usuario = $conn->fetchAll($this->preventInjection($sql));
        }
        
       
        $view = $this->renderView('AmapolannUserBundle:Account:usuario.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'usuario'=>$usuario
                
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    public function grillausuariosAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
               
        $id_usuario = $_SESSION["usuario"];
        
        $conn = $this->getDoctrine()->getConnection();  
        
        
        $nombre = "";
        if ($_POST){
            $nombre = $_POST["nombre"];
        }
      
        $sql = "SELECT * FROM usuario WHERE 1 = 1 AND habilitado = 1";
        
        if ($nombre!=""){
            $sql .= " AND nombre like '%$nombre%'";
        }
        
        $usuarios = $conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:usuarios.html.twig', array(
                'user'=>$user,
                'usuarios'=>$usuarios
        ));


        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }
    
    
    
    public function bannercotizacionAction()
    {
        $um = $this->get('user_manager');
                
        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $conn = $this->getDoctrine()->getConnection();        
        
        
        if(!$user)
            return $this->redirect($this->generateUrl('loginabm', array('error'=>'0')));
        
        $sql = "SELECT * FROM banner_cotizacion";


        $banners = $conn->fetchAll($this->preventInjection($sql));
        
        $view = $this->renderView('AmapolannUserBundle:Account:bannercotizacion.html.twig', array(
                'user'=>$user,
                'banners'=>$banners       
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function bannercotizacionabmAction()
    {
        $um = $this->get('user_manager');

        $user = $um->getLoggedInUser();
        
        $em = $this->getDoctrine()->getEntityManager();
        
        if(!$user)
            return $this->redirect($this->generateUrl('login', array('error'=>'0')));
        
        $conn = $this->getDoctrine()->getConnection();        
        $id_usuario = $_SESSION["usuario"];
        
        $id = 0;
       
        if (isset($_GET["id"])){
            $id = $_GET["id"];  
        }
        
        if (isset($_POST["id"])){
            $id = $_POST["id"];
        }
      
        if ($id!=0){
            $sql= "select 1 from carrousel_empresa where id='$id' ;";
            $existe = $conn->fetchColumn($this->preventInjection($sql));
            if ($existe!="1")
            {  
                return $this->redirect($this->generateUrl('grillacarrouselempresa'));
            }
           
        }        
      
        if ($_POST){
            $id = $_POST["id"];
            
            $titulo = (isset($_POST["titulo"])) ? $_POST["titulo"] : "";
            $subtitulo = (isset($_POST["subtitulo"])) ? $_POST["subtitulo"] : "";
            $orden = (isset($_POST["orden"])) ? $_POST["orden"] : "";
            
            
            
            if ($id == "0")
            {               
                $sql = "INSERT INTO `carrousel_empresa`
                        (`titulo`,
                        `subtitulo`,
                        `orden`)
                        VALUES
                        ('$titulo',
                        '$subtitulo',
                        $orden);
                        ";                
                
                $conn->exec($this->preventInjection($sql));
                
                $id=$conn->lastInsertId();                                
               
                return $this->redirect($this->generateUrl('carrouselempresaabm', array('id'=>$id)));
                
            }
            else{
                 
                 $sql = "UPDATE carrousel_empresa SET titulo = '$titulo' ,subtitulo= '$subtitulo' , orden = $orden WHERE id = '$id';";
                 $conn->exec($this->preventInjection($sql));
  
              
                return $this->redirect($this->generateUrl('grillacarrouselempresa'));
            }
            
            
        }
        
          
        $propiedad = null;
        $productos_coleccion = array();
       
        if ($id>0){
            $sql= "SELECT * FROM banner_cotizacion WHERE id = '$id';";

            $propiedad = $conn->fetchAll($this->preventInjection($sql));

            $id_usuario = $_SESSION["usuario"];
            
           
        }       
       
        
 
        $view = $this->renderView('AmapolannUserBundle:Account:imgbannercotizacion.html.twig', array(
                'user'=>$user,
                'id'=>$id,
                'id_usuario'=>$id_usuario,
                'propiedad'=>$propiedad,
        ));

        $response = new Response($view);

        $response->headers->set('Cache-Control', 'public, max-age=0');
        $response->headers->set('Pragma', 'no-cache'); 
        $response->setMaxAge(0);
        $response->setSharedMaxAge(0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('proxy-revalidate', true);

        return $response; 
    }

    public function portadabannercotizacionAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        
        $id_presentacion = $_POST["id_presentacion"];
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadabannercotizacion/imagenes/$id_presentacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadabannercotizacion/imagenes/$id_presentacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "UPDATE banner_cotizacion SET imagen = '$target_file' WHERE id='$id_presentacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }

    public function portadabannercotizacionmovilAction()
    {
        $conn = $this->getDoctrine()->getConnection();
        
        $id_presentacion = $_POST["id_presentacion"];
        
        
        if ($_FILES && (isset($_FILES["imagen_por"]["name"])&&($_FILES["imagen_por"]["name"])!=null&&($_FILES["imagen_por"]["name"]!=""))){
                  
                $target_file = "../portadabannercotizacion/imagenes/$id_presentacion/" . basename($_FILES["imagen_por"]["name"]);
                $rand = rand(1,999999);
                
                $directory = "../portadabannercotizacion/imagenes/$id_presentacion/";

                if(!is_dir($directory)){
                    mkdir($directory, 755, true);
                }
                $imageFileType = "jpg";
                $imageFileType=substr($target_file,strrpos($target_file,'.')+1);
                $target_file = $directory."_".$rand.".$imageFileType";
                if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ){
                    
                    //var_dump($size);die;
                    $target_file = strtolower($target_file);
                    $target_file = str_replace("jpeg", "jpg", $target_file);
                    $upload = move_uploaded_file($_FILES["imagen_por"]["tmp_name"], $target_file);
                     
                    if ($upload){
                           $sql = "UPDATE banner_cotizacion SET imagen_movil = '$target_file' WHERE id='$id_presentacion';";
                            $conn->exec($this->preventInjection($sql));
                            echo ("ok");die;
                    }
                } 
        }
        
        echo ("Selecciona una imágen.");die;
    }
 
}

class CifrasEnLetras {

  //---------------------------------------------
  // CONSTANTES
  const PREFIJO_ERROR = 'Error: ';
  const COMA = ',';
  const MENOS = '-';

  //---------------------------------------------
  // ENUMERACIONES
  const NEUTRO = 'neutro';
  const MASCULINO = 'masculino';
  const FEMENINO = 'femenino';

  //---------------------------------------------
  // GLOBAL
  public static $SEPARADOR_SEIS_CIFRAS = " ";

  //---------------------------------------------
  // LISTAS

  public static $listaUnidades = array( // Letras de los números entre el 0 y el 29
    "cero", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve",
    "diez", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve",
    "veinte", "veintiun", "veintidós", "veintitres", "veinticuatro", "veinticinco", "veintiseis", "veintisiete", "veintiocho", "veintinueve"
  );
  public static $listaDecenas = array( // Letras de las decenas
    "", "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa"
  );
  public static $listaCentenas = array ( // Letras de las centenas
    "", "cien", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos"
  );
  public static $listaOrdenesMillonSingular = array ( // Letras en singular de los órdenes de millón
    "", "millon", "billon", "trillon", "cuatrillon", "quintillon",
    "sextillon", "septillon", "octillon", "nonillon", "decillon",
    "undecillon", "duodecillon", "tridecillon", "cuatridecillon", "quidecillon",
    "sexdecillon", "septidecillon", "octodecillon", "nonidecillon", "vigillon"
  );
  public static $listaOrdenesMillonPlural = array ( // Letras en plural de los órdenes de millón
    "", "millones", "billones", "trillones", "cuatrillones", "quintillones",
    "sextillones", "septillones", "octillones", "nonillones", "decillones",
    "undecillones", "duodecillones", "tridecillones", "cuatridecillones", "quidecillones",
    "sexdecillones", "septidecillones", "octodecillones", "nonidecillones", "vigillones"
  );

  // ORDINALES (Wikipedia: "Nombres de los números en español", "Número ordinal")

  public static $listaUnidadesOrdinales = array ( // Letras de los ordinales entre 0º y 19º
    "ningun", "primer", "segundo", "tercer", "cuarto", "quinto", "sexto", "septimo", "octavo", "noveno",
    "decimo", "undecimo", "duodecimo", "decimotercer", "decimocuarto", "decimoquinto", "decimosexto", "decimoseptimo", "decimoctavo", "decimonoveno"
  );
  public static $listaDecenasOrdinales = array ( // Letras de las decenas ordinales
    "", "decimo", "vigesimo", "trigesimo", "cuadragesimo", "quincuagesimo", "sexagesimo", "septuagesimo", "octogesimo", "nonagesimo"
  );
  public static $listaCentenasOrdinales = array ( // Letras de las centenas ordinales
    "", "centesimo", "ducentesimo", "tricentesimo", "cuadringentesimo", "quingentesimo", "sexcentéeimo", "septingentesimo", "octingentesimo", "noningentésimo"
  );
  public static $listaPotenciasDiezOrdinales = array ( // Letras de las potencias de diez ordinales
    "", "eécimo", "centesimo", "milesimo", "diezmilesimo", "cienmilesimo", "millonesimo"
  );

  //---------------------------------------------
  // MÉTODOS PRINCIPALES

  /*
    Convierte a letras los números entre 0 y 29
  */
  protected static function convertirUnidades($unidades, $genero='neutro') {
    if ($unidades == 1) {
      if ($genero == 'masculino') return 'uno';
      elseif ($genero == 'femenino') return 'una';
    }
    else if ($unidades == 21) {
      if ($genero == 'masculino') return 'veintiuno';
      elseif ($genero == 'femenino') return 'veintiuna';
    }
    return self::$listaUnidades[$unidades];
  }

  /*
    Convierte a letras las centenas
  */
  protected static function convertirCentenas($centenas, $genero='neutro') {
    $resultado = self::$listaCentenas[$centenas];
    if ($genero == 'femenino') $resultado = str_replace('iento','ienta', $resultado);
    return $resultado;
  }

  /*
    Primer centenar: del cero al noventa y nueve
  */
  public static function convertirDosCifras($cifras, $genero='neutro') {
    $unidad = $cifras % 10;
    $decena = intval($cifras / 10);
    if ($cifras < 30) return self::convertirUnidades($cifras, $genero);
    elseif ($unidad == 0) return self::$listaDecenas[$decena];
    else return self::$listaDecenas[$decena].' y '.self::convertirUnidades($unidad, $genero);
  }

  /*
    Primer millar: del cero al novecientos noventa y nueve
  */
  public static function convertirTresCifras($cifras, $genero='neutro') {
    $decenas_y_unidades = $cifras % 100;
    $centenas = intval($cifras / 100);
    if ($cifras < 100) return self::convertirDosCifras($cifras, $genero);
    elseif ($decenas_y_unidades == 0) return self::convertirCentenas($centenas, $genero);
    elseif ($centenas == 1) return 'ciento '.self::convertirDosCifras($decenas_y_unidades, $genero);
    else return self::convertirCentenas($centenas, $genero).' '.self::convertirDosCifras($decenas_y_unidades, $genero);
  }

  /*
    Primer millón: del cero al novecientos noventa y nueve mil noventa y nueve
  */
  public static function convertirSeisCifras($cifras, $genero='neutro') {
    $primer_millar = $cifras % 1000;
    $grupo_miles = intval($cifras / 1000);
    $genero_miles = $genero=='masculino'? 'neutro': $genero;
    if ($grupo_miles == 0) return self::convertirTresCifras($primer_millar, $genero);
    elseif ($grupo_miles == 1) {
      if ($primer_millar == 0) return 'mil';
      else return 'mil '.self::convertirTresCifras($primer_millar, $genero);
    }
    elseif ($primer_millar == 0) return self::convertirTresCifras($grupo_miles, $genero_miles).' mil';
    else return self::convertirTresCifras($grupo_miles, $genero_miles).' mil '.self::convertirTresCifras($primer_millar, $genero);
  }

  /*
    Números enteros entre el cero y novecientos noventa y nueve mil novecientos noventa y nueve vigillones... etc, etc.
    Es decir entre el 0 y el (10^126)-1 o bien números entre 1 y 126 cifras.
    Las cifras por debajo del millón pueden ir en masculino o en femenino.
  */
  public static function convertirCifrasEnLetras($cifras, $genero='neutro', $separadorGruposSeisCifras=null) {

    // Inicialización
    $cifras = trim($cifras);
    $numeroCifras = strlen($cifras);
    if ($separadorGruposSeisCifras == null) $separadorGruposSeisCifras = self::$SEPARADOR_SEIS_CIFRAS;

    // Comprobación
    if ($numeroCifras == 0) return self::PREFIJO_ERROR.'No hay ningún número';
    for ($indiceCifra=0; $indiceCifra<$numeroCifras; $indiceCifra++) {
      $cifra = substr($cifras, $indiceCifra, 1);
      $esDecimal = strpos('0123456789', $cifra) !== false;
      if (!$esDecimal) return self::PREFIJO_ERROR.'Uno de los caracteres no es una cifra decimal';
    }
    if ($numeroCifras > 126) return self::PREFIJO_ERROR.'El número es demasiado grande ya que tiene más de 126 cifras';

    // Preparación
    $numeroGruposSeisCifras = intval($numeroCifras / 6) + (($numeroCifras % 6)? 1: 0);
    $cerosIzquierda = str_repeat('0', $numeroGruposSeisCifras*6 - $numeroCifras);
    $cifras = $cerosIzquierda.$cifras;
    $ordenMillon = $numeroGruposSeisCifras - 1;

    // Procesamiento
    $resultado = array();
    for ($indiceGrupo=0; $indiceGrupo<$numeroGruposSeisCifras*6; $indiceGrupo+=6) {
      $seisCifras = substr($cifras, $indiceGrupo, 6);

      if ($seisCifras != 0) {
        if (count($resultado) > 0) $resultado[] = $separadorGruposSeisCifras;

        if ($ordenMillon == 0) {
          $resultado[] = self::convertirSeisCifras($seisCifras, $genero);
        }
        elseif ($seisCifras == 1) {
          $resultado[] = 'un '.self::$listaOrdenesMillonSingular[$ordenMillon];
        }
        else {
          $resultado[] = self::convertirSeisCifras($seisCifras, 'neutro').' '.
                         self::$listaOrdenesMillonPlural[$ordenMillon];
        }
      }
      $ordenMillon--;
    }

    // Finalización
    if (count($resultado) == 0) $resultado[] = self::$listaUnidades[0];
    return implode('', $resultado);
  }

  public static function convertirCifrasEnLetrasMasculinas($cifras) {
    return self::convertirCifrasEnLetras($cifras, "masculino");
  }

  public static function convertirCifrasEnLetrasFemeninas($cifras) {
    return self::convertirCifrasEnLetras($cifras, "femenino");
  }

  /*
    Expresa un número con decimales y signo en letras acompañado del tipo de medida
    para la parte entera y la parte decimal.
    - Los caracters no numéricos son ignorados.
    - Los múltiplos de millón tienen la preposición 'de' antes de la palabra.
    - El género masculino o femenino sólo puede influir en las cifras inferiores al millón.
  */
  public static function convertirNumeroEnLetras(
    $cifras, $numeroDecimales=-1,
    $palabraEntera='', $palabraEnteraPlural='', $esFemeninaPalabraEntera=false,
    $palabraDecimal='', $palabraDecimalPlural='', $esFemeninaPalabraDecimal=false)
  {
    // Argumentos
    $cifras = is_float($cifras)? str_replace(".", self::COMA, $cifras): "$cifras";
    $palabraEnteraPlural = ($palabraEnteraPlural=='')? $palabraEntera.'s': $palabraEnteraPlural;
    $palabraDecimalPlural = ($palabraDecimalPlural=='')? $palabraDecimal.'s': $palabraDecimalPlural;

    // Limpieza
    $cifras = self::dejarSoloCaracteresDeseados($cifras, '0123456789' . self::COMA . self::MENOS);

    // Comprobaciones
    $repeticionesMenos = substr_count($cifras, self::MENOS);
    $repeticionesComa = substr_count($cifras, self::COMA);
    if ($repeticionesMenos > 1 || ($repeticionesMenos == 1 && !self::empiezaPor($cifras, self::MENOS)) ) {
      return self::PREFIJO_ERROR . 'Símbolo negativo incorrecto o demasiados símbolos negativos';
    }
    else if ($repeticionesComa > 1) {
      return self::PREFIJO_ERROR . 'Demasiadas comas decimales';
    }

    // Negatividad
    $esNegativo = self::empiezaPor($cifras, self::MENOS);
    if ($esNegativo) $cifras = substr($cifras, 1);

    // Preparación
    $posicionComa = strpos($cifras, self::COMA);
    if ($posicionComa === false) $posicionComa = strlen($cifras);

    $cifrasEntera = substr($cifras, 0, $posicionComa);
    if ($cifrasEntera == "" || $cifrasEntera == self::MENOS) $cifrasEntera = "0";
    $cifrasDecimal = substr($cifras, min($posicionComa + 1, strlen($cifras)));

    $esAutomaticoNumeroDecimales = $numeroDecimales < 0;
    if ($esAutomaticoNumeroDecimales) {
      $numeroDecimales = strlen($cifrasDecimal);
    }
    else {
      $cifrasDecimal = substr($cifrasDecimal, 0, min($numeroDecimales, strlen($cifrasDecimal)));
      $cerosDerecha = str_repeat('0', $numeroDecimales - strlen($cifrasDecimal));
      $cifrasDecimal = $cifrasDecimal . $cerosDerecha;
    }

    // Cero
    $esCero = self::dejarSoloCaracteresDeseados($cifrasEntera,"123456789") == "" &&
      self::dejarSoloCaracteresDeseados($cifrasDecimal,"123456789") == "";

    // Procesar
    $resultado = array();

    if ($esNegativo && !$esCero) $resultado[]= "menos ";

    $parteEntera = self::procesarEnLetras($cifrasEntera,
      $palabraEntera, $palabraEnteraPlural, $esFemeninaPalabraEntera);
    if (self::empiezaPor($parteEntera, self::PREFIJO_ERROR)) return $parteEntera;
    $resultado[]= $parteEntera;

    if ($cifrasDecimal != "") {
      $parteDecimal = self::procesarEnLetras($cifrasDecimal,
        $palabraDecimal, $palabraDecimalPlural, $esFemeninaPalabraDecimal);
      if (self::empiezaPor($parteDecimal, self::PREFIJO_ERROR)) return $parteDecimal;
      $resultado[]= " con ";
      $resultado[]= $parteDecimal;
    }

    return implode('', $resultado);
  }

  /*
    Convertir euros en letras

    Ejemplos:
      CifrasEnLetras::convertirEurosEnLetras("44276598801,2",2) --> "cuatrocientos noventa y ocho mil un euros con veinte céntimos"
      CifrasEnLetras::convertirEurosEnLetras(85009) --> "ochenta y cinco mil nueve euros"
      CifrasEnLetras::convertirEurosEnLetras(10200.35) --> "diez mil doscientos euros con treinta y cinco céntimos"
  */
  public static function convertirEurosEnLetras($euros, $numeroDecimales=2) {
    return self::convertirNumeroEnLetras($euros, $numeroDecimales,
      "euro", "euros", false, "céntimo", "céntimos", false);
  }

  /*
    Separa las cifras en grupos de 6 con subrayados y los grupos de 6 en grupos de 2 con punto
    Ejemplos: CifrasEnLetras::formatearCifras("-4739249,2") --> "-4_739.249,2"
   */
  public static function formatearCifras($cifras, $formato="") {
    $cifras = self::dejarSoloCaracteresDeseados("$cifras", "0123456789" . self::COMA . self::MENOS);

    if (strlen($cifras) == 0) return $cifras;

    $esNegativo = self::empiezaPor($cifras, self::MENOS);
    if ($esNegativo) $cifras = substr($cifras, 1);

    $posicionComa = strpos($cifras, self::COMA);
    $esDecimal = $posicionComa !== false;

    if (!$esDecimal) $posicionComa = strlen($cifras);
    $cifrasEntera = substr($cifras, 0, $posicionComa);
    $cifrasDecimal = "";

    if ($esDecimal) $cifrasDecimal = substr($cifras, min($posicionComa + 1, strlen($cifras)));
    if ($cifrasEntera == "") $cifrasEntera = "0";

    $resultado = array();
    $numeroCifras = strlen($cifrasEntera);
    $par = true;
    $contador = 1;

    for ($indice=0; $indice<$numeroCifras; $indice+=3) {
      $indiceGrupo = $numeroCifras - $indice;
      $tresCifras = substr($cifras, max($indiceGrupo-3,0), min(3, $numeroCifras-$indice));

      if ($indice > 0) {
        switch ($formato) {
          case 'html':
            $resultado[]= $par? ".": "<sub>$contador</sub>";
            if (!$par) $contador++;
            break;
          default:
            $resultado[]= $par? '.': '_';
        }
        $par = !$par;
      }
      $resultado[]= $tresCifras;
    }

    if ($esNegativo) $resultado[]= self::MENOS;

    $resultado = array_reverse($resultado);

    if ($esDecimal) {
      $resultado[]= self::COMA;
      $resultado[]= $cifrasDecimal;
    }

    return implode('', $resultado);
  }


  //---------------------------------------------
  // MÉTODOS AUXILIARES

  /*
    Borra todos los caracteres del texto que no sea alguno de los caracteres deseados.
    Ejemplos:
      self::dejarSoloCaracteresDeseados("89.500.400","0123456789") --> "89500400"
      self::dejarSoloCaracteresDeseados("ABC-000-123-X-456","0123456789") --> "000123456"
  */
  private static function dejarSoloCaracteresDeseados($texto, $caracteresDeseados) {
    $resultado = array();
    for ($indice = 0; $indice < strlen($texto); $indice++) {
      $caracter = $texto[$indice];
      if (strpos($caracteresDeseados, $caracter) !== false) $resultado[]= $caracter;
    }
    return implode('', $resultado);
  }

  /*
    Función auxiliar de convertirNumeroEnLetras
    para procesar por separado la parte entera y la decimal
  */
  private static function procesarEnLetras($cifras, $palabraSingular, $palabraPlural, $esFemenina) {
    // Género
    $genero = "neutro";
    if ($esFemenina) $genero = "femenino";
    else if ($palabraSingular == "") $genero = "masculino";

    // Letras
    $letras = self::convertirCifrasEnLetras($cifras, $genero);
    if (self::empiezaPor($letras, self::PREFIJO_ERROR)) return $letras;

    // Propiedades
    $esCero = $letras == self::convertirUnidades(0, $genero) || $letras == "";
    $esUno = $letras == self::convertirUnidades(1, $genero);
    $esMultiploMillon = !$esCero && self::acabaPor($cifras, "000000");

    // Palabra
    $palabra = "";
    if ($palabraSingular != "") {
      if ($esUno || $palabraPlural == "")
        $palabra = $palabraSingular;
      else
        $palabra = $palabraPlural;
    }

    // Resultado
    $resultado = array();
    $resultado[]= $letras;
    if ($palabra != "") {
      $resultado[]= $esMultiploMillon? " de ": " ";
      $resultado[]= $palabra;
    }
    return implode('', $resultado);
  }


  private static function empiezaPor($texto, $prefijo) {
    //return strstr($texto, $prefijo) !== false;
    return substr($texto, 0, strlen($prefijo)) === $prefijo;
  }

  private static function acabaPor($texto, $sufijo) {
    /*$largoTexto = strlen($texto);
    $largoSufijo = strlen($sufijo);
    if ($largoSufijo > $largoTexto) return false;
    return substr_compare($texto, $sufijo, -$largoSufijo) === 0;*/
    return substr($texto, -strlen($sufijo)) == $sufijo;
  }


} // class CifrasEnLetras
