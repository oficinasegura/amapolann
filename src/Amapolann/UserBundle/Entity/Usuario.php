<?php

namespace Amapolann\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Normalizer\NormalizableInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario")
 */
class Usuario 
{
    

        /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $email;
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

     /**
     * @ORM\Column(type="string", length=100)
     */
    protected $nombre;
    
    public function getNombre()
    {
        return $this->nombre;
    }
    
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $contrasena;
    
    public function getContrasena()
    {
        return $this->contrasena;
    }
    
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $habilitado;
    
    public function getHabilitado()
    {
        return $this->habilitado;
    }
    
    public function setHabilitado($habilitado)
    {
        $this->habilitado = $habilitado;
    }
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $admin;
    
    public function getAdmin()
    {
        return $this->admin;
    }
    
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    
    
    public function __construct() {
        
    }
    

    


   
}
