<?php
namespace MasPropiedades\UserBundle\Twig\Extension;

class UserExtension extends \Twig_Extension {
    private $um;
    public function __construct($um)
    {
        $this->um = $um;
    }

    /**
     * Returns a list of global functions to add to the existing list.
     *
     * @return array An array of global functions
     */
    public function getFunctions() {
        return array(
            'isFollowing'  => new \Twig_Function_Method($this, 'isFollowing', array(
                'is_safe' => array('html')
            )),
            'isFollowed'  => new \Twig_Function_Method($this, 'isFollowed', array(
                'is_safe' => array('html')
            )),
            'isFriend'  => new \Twig_Function_Method($this, 'isFriend', array(
                'is_safe' => array('html')
            )),
            'isBlocked'  => new \Twig_Function_Method($this, 'isBlocked', array(
                'is_safe' => array('html')
            )),
            'islogged'  => new \Twig_Function_Method($this, 'islogged', array(
                'is_safe' => array('html')
            )),
            'getloggedUsername'  => new \Twig_Function_Method($this, 'getloggedUsername', array(
                'is_safe' => array('html')
            )),
            'getloggedUser'  => new \Twig_Function_Method($this, 'getloggedUser', array(
                'is_safe' => array('html')
            )),
            'isAdmin'  => new \Twig_Function_Method($this, 'isAdmin', array(
                'is_safe' => array('html')
            )),
            'isNotiTypeInactive' =>  new \Twig_Function_Method($this, 'isNotiTypeInactive', array(
                'is_safe' => array('html')
            )),
            'getSlug' =>  new \Twig_Function_Method($this, 'getSlug', array(
                'is_safe' => array('html')
            )),
            'extractUrl' =>  new \Twig_Function_Method($this, 'extractUrl', array(
                'is_safe' => array('html')
            )),
        );
    }
    public function getSlug($text){
        
        $result = strtolower($text);
        $result = str_replace("á", "a", $result);
            $result = str_replace("é", "e", $result);
            $result = str_replace("í", "i", $result);
            $result = str_replace("ó", "o", $result);
            $result = str_replace("ú", "u", $result);

            $result = str_replace("Á", "a", $result);
            $result = str_replace("É", "e", $result);
            $result = str_replace("Í", "i", $result);
            $result = str_replace("Ó", "o", $result);
            $result = str_replace("Ú", "u", $result);

            $result = str_replace(" ", "-", $result);
            $result = str_replace("'", "", $result);
            $result = str_replace("%", "", $result);
        $result = preg_replace("/[^a-z0-9\s-.]/", "", $result);
        $result = trim(preg_replace("/[\s-]+/", " ", $result));
        $result = preg_replace("/\s/", "-", $result);
        return $result;
    }
    public function extractUrl($text){
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
//        var_dump($text); die;
        
        // Check if there is a url in the text
        if(preg_match($reg_exUrl, $text, $url)) {
           // make the urls hyper links
           return preg_replace($reg_exUrl, '<a target="_blank" href="'.$url[0].'" rel="nofollow">'.$url[0].'</a>', $text);
        } else {
           // if no urls in the text just return the text
           return $text;

        }
    }
    
    public function isFollowing($user) {
        return $this->um->isFollowing($user);
    }

    public function isFollowed($user) {
        return $this->um->isFollowed($user);
    }
    
    public function isFriend($user){
        return $this->um->isFriend($user);
    }
    
    public function isBlocked($user, $user1) {
        return $this->um->isBlocked($user,$user1);
    }
    
    public function islogged() {
        $user =  $this->um->getLoggedInUser();
        if($user)
            return true;
        else
            return false;
    }
    public function getloggedUser() {
         $user =  $this->um->getLoggedInUser();
         if(!$user) return false;
         return $user->getId();
    }
    
    public function getloggedUsername(){
        $user =  $this->um->getLoggedInUser();
        return $user->getNickname();
    }
    
    public function isAdmin(){
        return $this->um->isAdmin();
    }
    
    

    
    /*
     * Checks User single Notification conf
     */
    public function isNotiTypeInactive($nid, $channel){
        return $this->um->checkNotiType($nid, $channel);
    }
    
    public function getName()
    {
        return 'user_extension';
    }

}
?>
