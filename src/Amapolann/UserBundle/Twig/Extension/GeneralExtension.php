<?php
namespace Amapolann\UserBundle\Twig\Extension;

class GeneralExtension extends \Twig_Extension {
    
    public function __construct()
    {
       
    }
    
    public function getFilters() {
        return array(
            'nice_string'   => new \Twig_Filter_Method($this, 'niceString'),
            'slug'   => new \Twig_Filter_Method($this, 'slug'),
        );
    }
    
    public function getFunctions()
    {
        return array(
            'url_encode'  => new \Twig_Function_Method($this, 'urlEncode', array(
                'is_safe' => array('html')
            )),
            'isMobile'  => new \Twig_Function_Method($this, 'isMobile', array(
                'is_safe' => array('html')
            )),
        );
    }
    
    public function niceString($string){
        $substring = explode(' ', $string);
        $string = '';
        
        foreach($substring as $sstr){
           if(strlen($string) > 0)
            $string .= ' '.ucfirst(strtolower($sstr));
           else
            $string .= ucfirst(strtolower($sstr));
        }
        
        return $string; 
    }
    
    public function urlEncode($string){
        return urlencode($string);
    }
    
    public function getName()
    {
        return 'general_extension';
    }
    
    public function slug($string){
        $string = strtolower($string);
        
        $string = str_replace("á", "a", $string);
        $string = str_replace("é", "e", $string);
        $string = str_replace("í", "i", $string);
        $string = str_replace("ó", "o", $string);
        $string = str_replace("ú", "u", $string);
        $string = str_replace("Á", "a", $string);
        $string = str_replace("É", "e", $string);
        $string = str_replace("Í", "i", $string);
        $string = str_replace("Ó", "o", $string);
        $string = str_replace("Ú", "u", $string);
        $string = str_replace(" ", "-", $string);
        $string = str_replace("&", "_", $string);
        return $string;
    }
    
    public function isMobile(){
        return $this->check_user_agent('mobile');
    }
    
    function check_user_agent ( $type = NULL ) {
        $user_agent = strtolower ( $_SERVER['HTTP_USER_AGENT'] );
       // var_dump($user_agent);
        if ( $type == 'bot' ) {
                // matches popular bots
                if ( preg_match ( "/googlebot|adsbot|yahooseeker|yahoobot|msnbot|watchmouse|pingdom\.com|feedfetcher-google/", $user_agent ) ) {
                        
                    return false;
                        // watchmouse|pingdom\.com are "uptime services"
                }
        } else if ( $type == 'browser' ) {
                // matches core browser types
                if ( preg_match ( "/mozilla\/|opera\//", $user_agent ) ) {
                        return false;
                }
        } else if ( $type == 'mobile' ) {
                // matches popular mobile devices that have small screens and/or touch inputs
                // mobile devices have regional trends; some of these will have varying popularity in Europe, Asia, and America
                // detailed demographics are unknown, and South America, the Pacific Islands, and Africa trends might not be represented, here
                if ( preg_match ( "/phone|iphone|itouch|ipod|symbian|android|htc_|htc-|palmos|blackberry|opera mini|iemobile|windows ce|nokia|fennec|hiptop|kindle|mot |mot-|webos\/|samsung|sonyericsson|^sie-|nintendo/", $user_agent ) ) {
                        // these are the most common
                        return true;
                }
                else if ( preg_match ( "/ipad/", $user_agent ) ) {
                        // these are less common, and might not be worth checking
                       // var_dump("4");
                        return false;
                }
                else if ( preg_match ( "/mobile|pda;|avantgo|eudoraweb|minimo|netfront|brew|teleca|lg;|lge |wap;| wap /", $user_agent ) ) {
                        // these are less common, and might not be worth checking
                        return true;
                }
        }
        return false;
}
}
?>
