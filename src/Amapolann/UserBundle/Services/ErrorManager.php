<?php
/*
 * User Manager
 * 
 */

namespace Amapolann\UserBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\SessionHelper;


use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;


//esto es para poder colgarse del evento de inicio del sistema
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Serinco\CoreBundle\Entity\Usuario;

class ErrorManager
{
    
    private $em;
    private $dbal;
    private $session;
    
    
    public function __construct($templating)
    { 
        $this->templating = $templating;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        
        //var_dump($exception->getMessage());die;
        $message = $exception->getMessage();
        
       // $_SESSION["message"] = $message;
        $titulo = "Se ha producido un error inesperado"; 
        if ($exception instanceof NotFoundHttpException) {
            $message = "";
            $titulo = "PARECE QUE ESTA PÁGINA NO EXISTE"; 
        }
        
       // $response = new RedirectResponse("http://127.0.0.1/oficinasegura/web/admin/error_page");
        
        var_dump($message);die;
        $view =  $this->templating->render('MasPropiedadesCoreBundle:Default:error.html.twig', array(
            'message' => $message,
            'titulo' => $titulo,
        ));


        $response = new Response($view);

        
        $event->setResponse($response);
        
    }
     
}
