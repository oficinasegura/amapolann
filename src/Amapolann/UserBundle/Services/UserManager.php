<?php
/*
 * User Manager
 * 
 */

namespace Amapolann\UserBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\SessionHelper;

//esto es para poder colgarse del evento de inicio del sistema
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\Event;

use Symfony\Component\HttpFoundation\Cookie;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Serinco\CoreBundle\Entity\Usuario;

class UserManager
{
    
    private $em;
    private $dbal;
    private $session;
    
    public function __construct(EntityManager $em, $dbal, $session) {
        $this->em = $em;
        $this->dbal = $dbal;
        $this->session = $session;
    }
    
    /*
     * Fetch Logged user ID
     */
    public function getIdUser(){
       // if(!isset($_COOKIE['login']))
         //   return false;
        //else if ($_COOKIE['login']!='true')
          //  return false;
        
        //return "1003";
        if(isset($_SESSION["usuario"]))
                return $_SESSION["usuario"];
        return false;
    }
    
    public function getLoggedInUser(){
        if(isset($_SESSION["usuario"]))
                return true;
        
        return false;
        
        //return $this->getUser($id);
    }
    
   
    /**
     * Devuelve un usuario por id
     * @param int $id 
     */
    public function getUser($id){
        if(!$id || $id <= 0) return false;
        
        $userrepos = $this->em->getRepository('SerincoCoreBundle:Usuario');
        $user = $userrepos->find($id);
        
        if(count($user) <= 0){
            
            return false;
        }
        return $user;
    }
        
    
    /**
     * Crea un nuevo usuario
     * @param \Mublet\CoreBundle\Entity\Account $user
     * @return \Mublet\CoreBundle\Entity\Account o false  
     */
    public function register(\Mublet\CoreBundle\Entity\Account $user){
        
        
        if(!$user->getNickname() || 
           !$user->getEmail()    || 
           !$user->getPassword() ||
           !$user->getRole() )
            return false;
        //var_dump($user); die;
        if($this->exists($user))
            return false;
        
        $user->setPassword(md5($user->getPassword()));
        $user->setIsActive(1);
        
        $this->em->persist($user);
        $this->em->flush();
        
        //echo("hola"); die;
        
        $accountExtended= new \Mublet\CoreBundle\Entity\AccountExtended();
        $accountExtended->setAccount($user);
        $this->em->persist($accountExtended);
        $this->em->flush();
        
        return true;
    
    }
    /*
     * Autentica un usuario en base a user y pass
     */
    public function authenticate($usernameOrEmail, $password){
        
             if(!empty($usernameOrEmail) && !empty($usernameOrEmail)){
                if(strrpos($usernameOrEmail, '@')){
                    $query = "SELECT u.* FROM usuario u 
                            WHERE u.email = :usernameOrEmail AND u.habilitado = 1";
                }else {
                    $query = "SELECT u.* FROM usuario u 
                            WHERE u.nombre = :usernameOrEmail AND u.habilitado = 1";
                }
                
                $stmt = $this->em->getConnection()->prepare($query);
                $stmt->execute([
                    'usernameOrEmail' => $usernameOrEmail
                ]);
                
                $results = $stmt->fetchAll();
                if(empty($results)){
                    return false;
                }

                $descifrado = openssl_decrypt($results[0]['contrasena'], 'AES-128-CBC', '8d11d7c9b5ee156af537c2fdd63adf40', 0, '8d11d7c9b5ee156a');

                if($descifrado == $password){
                    $results[0]['contrasena'] = $password;
                } else {
                    return false;
                }
                
                if($results && count($results) > 0){
                    $user = $results[0];
                    $this->login($user);
                    return $user;
                }else
                    return false;
            }
            return false;
    }
    
    public function recover($usernameOrEmail){
        
             if(!empty($usernameOrEmail) && !empty($usernameOrEmail)){
                if(strrpos($usernameOrEmail, '@'))
                    $query = "select * from usuario where email = '{$usernameOrEmail}' and habilitado=1 ;";
                else
                    $query = "select * from usuario where id = '{$usernameOrEmail}' and habilitado=1 ;";
                
                
                $results = $this->em->getConnection()->fetchAll($query);
                
                if($results && count($results) > 0){
                    $user = $this->em->getRepository ('SerincoCoreBundle:Usuario')->find($results[0]['id']);
                    mail($user->getEmail(), "Recuperación de contraseña" , "Tu contraseña es: ".$user->getContrasena());
                    return $user;
                }else
                    return false;
            }
            return false;
    }
    
    public function esMiTasacion()
    {
        $idusuario = $_SESSION["usuario"];
        $id = $_SESSION["idtasacion"];
        $query = "select 1 from tasacion where idexterno = '$id' and idusuario='$idusuario' ;";
                
        //       
        $result = $this->em->getConnection()->fetchColumn($query);
        $rta = false;
        if ($result!=null && $result=="1")
        {
            $rta = true;
        }
        //var_dump($rta);die;
        return $rta;
        
               
    }
    
    /**
     * Guarda en session un StdObj que representa al usuario logeado
     * @param \Mublet\CoreBundle\Entity\Account $user
     * @return true
     */
    public function login($user){
        //$this->session->set('user', $this->toStd($user));
        
        
        $_SESSION["usuario"] = $user["id"];
        
        $idusuario = $user["id"];
        
       
        
        return true;
    }
    
    /**
     * Limpia la session
     * @return bool true 
     */
    public function logout(){
        
       
        foreach($_SESSION as $key=>$value)
            unset($_SESSION[$key]);
        
        $this->session->close();
        
        $this->session->clear();
        $this->session->invalidate();
        $this->session->__destruct();
        return true;
    }
    
    /*
     * Devuelve un stObject que representa al usuario
     */
    public function toStd($user){
        $stdObj = new \stdClass();

        $stdObj->id = $user->getId();
        $stdObj->username = $user->getNombre();
        $stdObj->email    = $user->getEmail();
        $stdObj->admin = $user->getAdmin();
        $stdObj->habilitado = $user->getHabilitado();

        return $stdObj;
    }
     
}
